/*
Navicat MySQL Data Transfer

Source Server         : db-yunmai-test
Source Server Version : 50615
Source Host           : 118.190.105.190:3306
Source Database       : dbyunmai_fvsvr

Target Server Type    : MYSQL
Target Server Version : 50615
File Encoding         : 65001

Date: 2018-06-14 15:37:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for fv_factory
-- ----------------------------
DROP TABLE IF EXISTS `fv_factory`;
CREATE TABLE `fv_factory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `pid` int(11) NOT NULL DEFAULT '0',
  `create_time`  datetime NULL DEFAULT CURRENT_TIMESTAMP  COMMENT '创建时间' ,
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='工厂-代工厂公司基础信息';

-- ----------------------------
-- Records of fv_factory
-- ----------------------------
INSERT INTO `fv_factory` VALUES ('1', '汇思科代工厂', '0', '2018-06-12 14:14:16', '2018-06-12 14:14:19');

-- ----------------------------
-- Table structure for fv_users
-- ----------------------------
DROP TABLE IF EXISTS `fv_users`;
CREATE TABLE `fv_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) DEFAULT NULL COMMENT '登录名称',
  `password` varchar(255) DEFAULT '' COMMENT '密码',
  `sex` tinyint(4) DEFAULT '0' COMMENT '1:男 2：女',
  `tel` varchar(255) DEFAULT '' COMMENT '联系电话',
  `factory_id` int(11) DEFAULT '1' COMMENT '工厂ID',
  `create_time`   datetime NULL DEFAULT CURRENT_TIMESTAMP  COMMENT '创建时间'  ,
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `ix_user_name` (`user_name`),
  KEY `ix_factory_id_name` (`factory_id`,`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='工厂--工厂用户表';

-- ----------------------------
-- Records of fv_users
-- ----------------------------
INSERT INTO `fv_users` VALUES ('1', 'hsk001', '111111', '0', '', '1', '2018-06-12 14:14:52', '2018-06-12 14:14:55');

-- ----------------------------
-- Table structure for fv_weightinfo
-- ----------------------------
DROP TABLE IF EXISTS `fv_weightinfo`;
CREATE TABLE `fv_weightinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `factory_id` int(11) NOT NULL COMMENT '工厂ID',
  `device_name` varchar(255) NOT NULL DEFAULT 'UNKNOWN_DEVICE' COMMENT '设备名称',
  `mac_no` varchar(255) NOT NULL DEFAULT '' COMMENT 'Mac地址',
  `device_no` varchar(255) NOT NULL COMMENT '设备ID',
  `platform` smallint(6) NOT NULL DEFAULT '0' COMMENT '1:IOS客户端 2:android客户端 10:WiFi数据上报 11:IOS WiFi确认数据 12:Android WiFi确认数据 0:其他',
  `weight` float NOT NULL DEFAULT '0' COMMENT '体重',
  `resistance` int(11) DEFAULT '0' COMMENT '电阻值',
  `fat` float DEFAULT '0' COMMENT '脂肪值',
  `timestamp` int(11) DEFAULT NULL COMMENT '创建时间时间戳',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime  DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间 ',
  PRIMARY KEY (`id`),
  KEY `ix_factory_user_id` (`factory_id`,`user_id`),
  KEY `ix_device_name` (`device_name`),
  KEY `ix_platform` (`platform`),
  KEY `ix_mac_no` (`mac_no`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='工厂--工厂测试APP上报体重数据';

-- ----------------------------
-- Records of fv_weightinfo
-- ----------------------------
INSERT INTO `fv_weightinfo` VALUES ('1', '1', '1', 'UNKNOWN_DEVICE', '111', '11', '1', '60', '300', '20.5', '1111111111', '2018-06-12 19:54:15', '2018-06-12 19:54:18');
INSERT INTO `fv_weightinfo` VALUES ('2', '1', '1', 'UNKNOWN_DEVICE', '22:AA:AA:SD:DD', '22:AA:AA:SD:DD', '1', '56.1', '300', '23.5', '1528805132', '2018-06-12 19:54:18', '2018-06-12 19:54:18');
INSERT INTO `fv_weightinfo` VALUES ('3', '1', '1', 'UNKNOWN_DEVICE', '22:AA:AA:SD:DD', '22:AA:AA:SD:DD', '1', '56.1', '300', '23.5', '1528805132', '2018-06-12 19:54:18', '2018-06-12 19:54:18');
INSERT INTO `fv_weightinfo` VALUES ('4', '1', '1', 'UNKNOWN_DEVICE', '22:AA:AA:SD:DD', '22:AA:AA:SD:DD', '1', '56', '334', '23.4', '1528805532', '2018-06-12 19:54:18', '2018-06-12 19:54:18');
INSERT INTO `fv_weightinfo` VALUES ('5', '2', '1', 'UNKNOWN_DEVICE', '22:AA:AA:SD:DD', '22:AA:AA:SD:DD', '1', '57', '334', '27.4', '1528805632', '2018-06-12 19:54:18', '2018-06-12 19:54:18');
INSERT INTO `fv_weightinfo` VALUES ('6', '3', '1', 'UNKNOWN_DEVICE', '22:AA:AA:SD:DD', '22:AA:AA:SD:DD', '1', '57', '334', '27.4', '1528805632', '2018-06-12 19:54:18', '2018-06-12 19:54:18');
INSERT INTO `fv_weightinfo` VALUES ('7', '4', '1', 'UNKNOWN_DEVICE', '22:AA:AA:SD:DD', '22:AA:AA:SD:DD', '1', '57', '334', '27.4', '1528805632', '2018-06-12 19:54:18', '2018-06-12 19:54:18');

-- ----------------------------
-- Table structure for sys_token
-- ----------------------------
DROP TABLE IF EXISTS `sys_token`;
CREATE TABLE `sys_token` (
  `user_id` bigint(20) NOT NULL,
  `token` varchar(100) NOT NULL COMMENT 'token',
  `expire_time` datetime DEFAULT NULL COMMENT '过期时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户Token';

-- ----------------------------
-- Records of sys_token
-- ----------------------------
INSERT INTO `sys_token` VALUES ('2', 'ae21aac6210d41629e5dd72ae76a725a', '2018-06-13 09:13:20', '2018-06-12 21:13:20');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `mobile` varchar(20) NOT NULL COMMENT '手机号',
  `password` varchar(64) DEFAULT NULL COMMENT '密码',
  `create_time`  datetime NULL DEFAULT CURRENT_TIMESTAMP  COMMENT '创建时间' ,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'ttt', '13612345678', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '2017-03-23 22:37:41');
INSERT INTO `sys_user` VALUES ('2', 'simi', '18520222723', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '2018-06-12 21:03:18');
