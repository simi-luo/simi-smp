# simi-smp-fvsvr

#### 项目介绍
simi-smp-fvsvr:工厂版本API项目
1. 对工厂APP数据上报和工厂测试进行跟踪和管理；
2. 规范工厂对生产每天设备进行检查跟踪；
3. 跟踪每个工厂不同产品周期性生产产量；


#### 软件架构
软件架构说明
主要分为三部分组成：
1. 工厂客户端APP：（iOS和安卓），提供工厂检测人员，连接硬件检测、硬件使用检测;
2. 工厂API上报项目：提供检测人员登录，检测设备数据上报和跟踪；
3. 工厂版本后台管理：提供公司查看数据、分析工厂检查情况、统计生产数据；


#### 安装教程


1. 程序打包 
   ：cd /d  D:\work\J2EE\workspace\simi-smp
    ：mvn clean package
2. xxxx 

#### 使用说明

1. xxxx
2. xxxx 
 

#### 接口描述
##### 统一权限限制
1、接口需求权限才能访问时，在请求时统一在header中带上token,token就是登录成功后生产的token值( **推荐使用** )
2、接口需求权限才能访问时，在请求时统一携带参数token,token就是登录成功后生产的token值


##### 登录接口
###### 请求方式：POST 
###### 请求地址：http://www.iyunmai.xin/fvsvr/ios/users/login.d  (iOS)
###### 请求地址：http://www.iyunmai.xin/fvsvr/android/users/login.d  (android)
###### 权限限制：无权限限制
###### 请求参数：
```
{userName}：String  登录名称 （必填）
{password}：String  登录密码 （必填） 
```
###### 返回内容：
```
{
    "msg": "success",
    "code": 0,
    "expire": 86399865,
    "user": {
        "mobile": "18520222723",
        "userId": 2,
        "factoryId": "18520222723",
        "username": "18520222723"
    },
    "token": "c01a8ff2346e45ab86037616ca933c03"
}

msg：状态描述
code：状态码 0：OK 
token:登录成功生产的token值，以后接口访问时使用的（header 头中使用的或携带token参数使用）
expire：过去时间
user.mobile:登录用户手机号码
user.userId:登录用户ID
user.username:登录用户名称
user.factoryId:登录用户工厂ID

```



##### 登出接口
###### 请求方式：POST 
###### 请求地址：http://www.iyunmai.xin/fvsvr/ios/users/logout.d  (iOS)
###### 请求地址：http://www.iyunmai.xin/fvsvr/android/users/logout.d  (android)
###### 权限限制：需求登录状态，携带token
###### 请求参数：
```
{userId}：long  用户Id （必填） 
```
###### 返回内容：

```
{
    "msg": "success",
    "code": 0
}
msg：状态描述
code：状态码 0：OK 
```



##### 单条体重相关数据上报
###### 请求方式：POST 
###### 请求地址：http://www.iyunmai.xin/fvsvr/ios/weight/save.d (iOS)
###### 请求地址：http://www.iyunmai.xin/fvsvr/android/weight/save.d (android)
###### 权限限制：需求登录状态，携带token
###### 请求参数：
```
{userId} : long 用户ID 
{factoryId} :int   工厂ID 
{deviceName} :String 设备名称 
{macNo} :String  Mac地址   
{deviceNo} :String  设备编号 
{platform} :int 平台类型   
{weight} :float  体重 
{resistance} :int  电阻  
{fat} :float  脂肪   
{timestamp} :int  数据产生截止时间
```
###### 返回内容：

```
{
    "msg": "success",
    "code": 0,
    "data": true
}
```


##### 批量体重相关数据上报
###### 请求方式：POST 
###### 请求地址：http://www.iyunmai.xin/fvsvr/ios/weight/save-batch.d (iOS)
###### 请求地址：http://www.iyunmai.xin/fvsvr/android/weight/save-batch.d (android)
###### 权限限制：需求登录状态，携带token
###### 请求参数：
```
@RequestBody String json
json:

    [{
    	"userId":1,
    	"factoryId":1,
    	"deviceName":"UNKNOWN_DEVICE",
    	"macNo":"22:AA:AA:SD:DD",
    	"deviceNo":"22:AA:AA:SD:DD",
    	"platform":1,
    	"weight":56.0,
    	"resistance":334,
    	"fat":23.4,
    	"timestamp":1528805532
    	},{
    	"userId":2,
    	"factoryId":1,
    	"deviceName":"UNKNOWN_DEVICE",
    	"macNo":"22:AA:AA:SD:DD",
    	"deviceNo":"22:AA:AA:SD:DD",
    	"platform":1,
    	"weight":57.0,
    	"resistance":334,
    	"fat":27.4,
    	"timestamp":1528805632
    	},{
    	"userId":3,
    	"factoryId":1,
    	"deviceName":"UNKNOWN_DEVICE",
    	"macNo":"22:AA:AA:SD:DD",
    	"deviceNo":"22:AA:AA:SD:DD",
    	"platform":1,
    	"weight":57.0,
    	"resistance":334,
    	"fat":27.4,
    	"timestamp":1528805632
    	},{
    	"userId":4,
    	"factoryId":1,
    	"deviceName":"UNKNOWN_DEVICE",
    	"macNo":"22:AA:AA:SD:DD",
    	"deviceNo":"22:AA:AA:SD:DD",
    	"platform":1,
    	"weight":57.0,
    	"resistance":334,
    	"fat":27.4,
    	"timestamp":1528805632
    	}
    ]
```

###### 返回内容：
```
{
    "msg": "success",
    "code": 0,
    "data": true
}
```


##### 获取BLE协议接口
###### 请求方式：POST 
###### 请求地址：http://www.iyunmai.xin/fvsvr/ios/ble/list.json  (iOS)
###### 请求地址：http://www.iyunmai.xin/fvsvr/android/ble/list.json  (android)
###### 权限限制：登录权限限制
###### 请求参数：
```
无
```
###### 返回内容：
```
{
  "msg": "success",
  "code": 0,
  "data": [
    {
      "id": 1,
      "name": "222",
      "data": "222"
    }
  ]
}

msg：状态描述
code：状态码 0：OK 
name：协议名称
data：协议内容
```

==========================================================================



