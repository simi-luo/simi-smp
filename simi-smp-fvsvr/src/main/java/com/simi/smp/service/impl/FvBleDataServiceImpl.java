package com.simi.smp.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.simi.smp.entity.FvBleDataEntity;
import com.simi.smp.mapper.FvBleDataMapper;
import com.simi.smp.service.FvBleDataService; 

@Service
public class FvBleDataServiceImpl  extends ServiceImpl<FvBleDataMapper, FvBleDataEntity> implements FvBleDataService {

}
