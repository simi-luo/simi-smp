/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.simi.smp.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.simi.core.utils.CodeUtils;
import com.simi.core.utils.DateUtils;
import com.simi.smp.entity.SysTokenEntity;
import com.simi.smp.mapper.SysTokenMapper;
import com.simi.smp.service.SysTokenService;

import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

 
/**
* <b>Description:</b><br>
*  系统统一Token 管理
*
* @author:  holele
* @version:  1.0
* @Note
* <b>ProjectName:</b> simi-smp-fvsvr
* <br><b>PackageName:</b> com.simi.smp.service.impl
* <br><b>ClassName:</b> SysTokenServiceImpl
* <br><b>Date:</b> 2018年6月13日 下午4:01:47
*/
 
@Service
public class SysTokenServiceImpl extends ServiceImpl<SysTokenMapper, SysTokenEntity> implements SysTokenService {
	 
	/** @Fields EXPIRE_TIME :  过去时间多少个小时：24小时   */  
	private final static int EXPIRE_TIME = 24;

	@Override
	public SysTokenEntity queryByToken(String token) {
		return this.selectOne(new EntityWrapper<SysTokenEntity>().eq("token", token));
	}

	@Override
	public SysTokenEntity createToken(long userId) {  
	    Date now =new Date();
        Date expireTime = DateUtils.addDateHours(now, EXPIRE_TIME);
		String token = CodeUtils.getToken(); 
		SysTokenEntity tokenEntity = new SysTokenEntity();
		tokenEntity.setUserId(userId);
		tokenEntity.setToken(token);
		tokenEntity.setUpdateTime(now);
		tokenEntity.setExpireTime(expireTime);
		this.insertOrUpdate(tokenEntity); 
		return tokenEntity;
	}

	@Override
	public void expireToken(long userId){
		Date now = new Date();

		SysTokenEntity tokenEntity = new SysTokenEntity();
		tokenEntity.setUserId(userId);
		tokenEntity.setUpdateTime(now);
		tokenEntity.setExpireTime(now);
		this.insertOrUpdate(tokenEntity);
	}

 
}
