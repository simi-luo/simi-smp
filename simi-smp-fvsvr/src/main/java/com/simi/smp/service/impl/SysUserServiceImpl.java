/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.simi.smp.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.simi.core.exception.SimiException;
import com.simi.smp.entity.SysTokenEntity;
import com.simi.smp.entity.SysUserEntity;
import com.simi.smp.form.LoginForm;
import com.simi.smp.mapper.SysUserMapper;
import com.simi.smp.service.SysTokenService;
import com.simi.smp.service.SysUserService;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service; 

import java.util.HashMap;
import java.util.Map; 


//@Service("SysTokenService")
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUserEntity> implements SysUserService {
	
	@Autowired
	private SysTokenService tokenService;

	@Override
	public SysUserEntity queryByUserName(String userName) {
		SysUserEntity userEntity = new SysUserEntity();
		userEntity.setUsername(userName);
		return baseMapper.selectOne(userEntity);
	}

	@Override
	public Map<String, Object> login(LoginForm form) {
		SysUserEntity user = queryByUserName(form.getUserName()); 
		if(null==user) {
			throw new SimiException("手机号或密码错误1");
		}
		//ObjectUtils.isNull(user, "手机号或密码错误1");
 
		if(!user.getPassword().equals(DigestUtils.sha256Hex(form.getPassword()))){
			throw new SimiException("手机号或密码错误2");
		}

		//获取登录token
		SysTokenEntity tokenEntity = tokenService.createToken(user.getUserId()); 
		Map<String, Object> map = new HashMap<>(3);
		Map<String, Object> mapUser = new HashMap<>(3);
		mapUser.put("userId", user.getUserId());
        mapUser.put("username", user.getUsername());
        mapUser.put("factoryId", user.getUsername());
        mapUser.put("mobile", user.getMobile());
        map.put("user",mapUser);
		map.put("token", tokenEntity.getToken());
		map.put("expire", tokenEntity.getExpireTime().getTime() - System.currentTimeMillis()); 
		return map;
	}

}
