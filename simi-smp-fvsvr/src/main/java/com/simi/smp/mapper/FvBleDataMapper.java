package com.simi.smp.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.simi.smp.entity.FvBleDataEntity;

public interface FvBleDataMapper extends BaseMapper<FvBleDataEntity> {

}
