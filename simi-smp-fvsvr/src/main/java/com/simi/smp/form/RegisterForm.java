/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.simi.smp.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;


 
/**
* <b>Description:</b><br>
*  注册表单
*
* @author:  holele
* @version:  1.0
* @Note
* <b>ProjectName:</b> simi-smp-fvsvr
* <br><b>PackageName:</b> com.simi.smp.form
* <br><b>ClassName:</b> RegisterForm
* <br><b>Date:</b> 2018年6月13日 下午1:39:56
*/
 
@ApiModel(value = "注册表单")
public class RegisterForm {
    @ApiModelProperty(value = "账号名称")
    @NotBlank(message="账号名称不能为空")
    private String userName;

    @ApiModelProperty(value = "密码")
    @NotBlank(message="密码不能为空")
    private String password;

    @ApiModelProperty(value = "手机")
    @NotBlank(message="手机号码不能为空")
    private String mobile;

    @ApiModelProperty(value = "工厂编号")
    @NotBlank(message="工厂编号不能为空")
    private Integer factoryId; 


    public String getMobile() {
      return mobile;
    }

    public void setMobile(String mobile) {
      this.mobile = mobile;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getFactoryId() {
      return factoryId;
    }

    public void setFactoryId(Integer factoryId) {
      this.factoryId = factoryId;
    }
    
}
