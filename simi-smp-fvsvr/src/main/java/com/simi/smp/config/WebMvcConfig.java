package com.simi.smp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.simi.smp.authorization.AuthorizationInterceptor;
import com.simi.smp.authorization.LoginUserHandlerMethodArgumentResolver;

import java.util.List;

 
/**
* <b>Description:</b><br>
* 公共WebMVC 配置
* @author:  holele
* @version:  1.0
* @Note
* <b>ProjectName:</b> simi-smp-fvsvr
* <br><b>PackageName:</b> com.simi.smp.config
* <br><b>ClassName:</b> WebMvcConfig
* <br><b>Date:</b> 2018年6月13日 下午1:12:22
*/ 
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
	
    @Autowired
    private AuthorizationInterceptor authorizationInterceptor;
    
    @Autowired
    private LoginUserHandlerMethodArgumentResolver loginUserHandlerMethodArgumentResolver;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authorizationInterceptor).addPathPatterns("/api/**");
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(loginUserHandlerMethodArgumentResolver);
    }
}