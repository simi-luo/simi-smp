package com.simi.smp.config;


import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

/**
* <b>Description:</b><br>
*  生产统一API文档配置
*
* @author:  holele
* @version:  1.0
* @Note
* <b>ProjectName:</b> simi-smp-fvsvr
* <br><b>PackageName:</b> com.simi.smp.config
* <br><b>ClassName:</b> SwaggerConfig
* <br><b>Date:</b> 2018年6月13日 下午1:10:18
*/
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    /**   
    * <b>Description:</b><br>
    *创建API文档
    * @MethodName: createRestApi    
    * @return Docket: 返回类型  
    */
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
            .apiInfo(selfCareApiInfo())
            .select() 
            .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class)) 
            .apis(RequestHandlerSelectors.basePackage("com.simi.smp.controller"))
            .paths(PathSelectors.any())
            .build()
            .securitySchemes(selfSecurity());
    }

    /**   
    * <b>Description:</b><br>
    * fvsvr API  文档基础信息配置
    * @MethodName: selfCareApiInfo    
    * @return ApiInfo: 返回类型  
    */
    private ApiInfo selfCareApiInfo() {
        return new ApiInfoBuilder()
            .title("fvsvr API ")
            .description("fvsvr API  文档")
            .termsOfServiceUrl("http://127.0.0.1")
            .version("3.2.0")
            .build();
    }

    /**   
    * <b>Description:</b><br> 
    * api key 配置
    * @MethodName: selfSecurity    
    * @return List<ApiKey>: 返回类型  
    */
    private List<ApiKey> selfSecurity() {
        return newArrayList(
            new ApiKey("token", "token", "header")
        );
    }

}