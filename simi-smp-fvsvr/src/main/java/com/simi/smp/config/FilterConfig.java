package com.simi.smp.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.simi.core.xxs.XssFilter;

import javax.servlet.DispatcherType;

 
/**
* <b>Description:</b><br>
*  过滤配置
*
* @author:  holele
* @version:  1.0
* @Note
* <b>ProjectName:</b> simi-smp-fvsvr
* <br><b>PackageName:</b> com.simi.smp.config
* <br><b>ClassName:</b> FilterConfig
* <br><b>Date:</b> 2018年6月13日 下午1:08:14
*/
 
@Configuration
public class FilterConfig {

    /**   
    * <b>Description:</b><br>
    * 描述这个方法的作用
    *
    * @MethodName: xssFilterRegistration  
    * @return: 说明 
    * @return FilterRegistrationBean: 返回类型  
    */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Bean
    public FilterRegistrationBean xssFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setDispatcherTypes(DispatcherType.REQUEST);
        registration.setFilter(new XssFilter());
        registration.addUrlPatterns("/*");
        registration.setName("xssFilter");
        return registration;
    }
}
