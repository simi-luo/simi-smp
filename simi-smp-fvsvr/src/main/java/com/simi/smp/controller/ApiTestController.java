package com.simi.smp.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.simi.core.utils.Result;
 
/**
* <b>Description:</b><br>
*  描述内容
*
* @author:  holele
* @version:  1.0
* @Note
* <b>ProjectName:</b> simi-smp-fvsvr
* <br><b>PackageName:</b> com.simi.smp.controller
* <br><b>ClassName:</b> ApiTestController
* <br><b>Date:</b> 2018年6月13日 下午1:25:32
*/ 
@RestController 
@Api(tags="测试接口")
public class ApiTestController {
  private static final Logger logger = LoggerFactory.getLogger(ApiTestController.class); 

    /**   
    * <b>Description:</b><br>
    * 测试接口
    * @MethodName: test  
    * @return: 说明 
    * @return Result: 返回类型  
    */
    @GetMapping("health.d")
    @ApiOperation("测试接口")
    public Result test(){
        logger.debug("debug:{}","debug..............");
        logger.info("info:{}","info..............");
        logger.error("error:{}","error..............");
        return Result.ok().put("msg", "test。。。");
    }

}
