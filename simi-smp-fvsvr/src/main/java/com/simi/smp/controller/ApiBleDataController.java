package com.simi.smp.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.bind.annotation.RestController;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.simi.core.annotation.LoginPermission;
import com.simi.core.utils.DateUtils;
import com.simi.core.utils.Result;
import com.simi.smp.entity.FvBleDataEntity;
import com.simi.smp.entity.FvWeightinfoEntity;
import com.simi.smp.entity.SysUserEntity;
import com.simi.smp.service.FvBleDataService;
import com.simi.smp.service.FvWeightinfoService;

import springfox.documentation.annotations.ApiIgnore;



 
/**
* <b>Description:</b><br>
*  描述内容
*
* @author:  holele
* @version:  1.0
* @Note
* <b>ProjectName:</b> simi-smp-fvsvr
* <br><b>PackageName:</b> com.simi.smp.controller
* <br><b>ClassName:</b> ApiWeightController
* <br><b>Date:</b> 2018年6月13日 下午1:21:55
*/
 
@RestController 
@RequestMapping({"/ios/ble","/android/ble"})
@Api(tags="体重上报接口")
public class ApiBleDataController {


  @SuppressWarnings("unused")
  private static final Logger logger = LoggerFactory.getLogger(ApiBleDataController.class); 
    @Autowired
    FvBleDataService fvBleDataService;
    
    @LoginPermission
    @GetMapping("list.json")
    @ApiOperation(value="获取测试数据", response=SysUserEntity.class)
    public Result list(){  
        //Wrapper<FvBleDataEntity> w = new Wrapper<FvBleDataEntity>();
        Wrapper<FvBleDataEntity>  wrapper = new EntityWrapper<FvBleDataEntity>();
        wrapper.isNotNull("name").and().isNotNull("data");
        List<FvBleDataEntity> list = fvBleDataService.selectList(wrapper);
        logger.debug("list:{}",list);
        return Result.ok().put("data", list);
    }
    

    
}
