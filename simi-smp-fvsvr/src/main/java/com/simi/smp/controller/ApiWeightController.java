package com.simi.smp.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.bind.annotation.RestController;
import com.alibaba.fastjson.JSON;
import com.simi.core.annotation.LoginPermission;
import com.simi.core.utils.DateUtils;
import com.simi.core.utils.Result; 
import com.simi.smp.entity.FvWeightinfoEntity;
import com.simi.smp.entity.SysUserEntity;
import com.simi.smp.service.FvWeightinfoService;

import springfox.documentation.annotations.ApiIgnore;



 
/**
* <b>Description:</b><br>
*  描述内容
*
* @author:  holele
* @version:  1.0
* @Note
* <b>ProjectName:</b> simi-smp-fvsvr
* <br><b>PackageName:</b> com.simi.smp.controller
* <br><b>ClassName:</b> ApiWeightController
* <br><b>Date:</b> 2018年6月13日 下午1:21:55
*/
 
@RestController 
@RequestMapping({"/ios/weight","/android/weight"})
@Api(tags="体重上报接口")
public class ApiWeightController {


  private static final Logger logger = LoggerFactory.getLogger(ApiWeightController.class); 
    @Autowired
    FvWeightinfoService fvWeightinfoService;
    
    @LoginPermission
    @GetMapping("list.json")
    @ApiOperation(value="获取测试数据", response=SysUserEntity.class)
    public Result list(){  
        return Result.ok().put("data", fvWeightinfoService.selectById(1)).put("password", DigestUtils.sha256Hex("admin"));
    }
    

    @LoginPermission
    @PostMapping("save.d")
    @ApiOperation(value="单条数据保持", response=FvWeightinfoEntity.class)
    public Result saveOne(@ApiIgnore  FvWeightinfoEntity vo){  
        vo.setCreateTime(new Date(vo.getTimestamp() * 1000));
        return Result.ok().put("data", fvWeightinfoService.insert(vo));
    }

    
//    [{
//    	"userId":1,
//    	"factoryId":1,
//    	"deviceName":"UNKNOWN_DEVICE",
//    	"macNo":"22:AA:AA:SD:DD",
//    	"deviceNo":"22:AA:AA:SD:DD",
//    	"platform":1,
//    	"weight":56.0,
//    	"resistance":334,
//    	"fat":23.4,
//    	"timestamp":1528805532
//    	},{
//    	"userId":2,
//    	"factoryId":1,
//    	"deviceName":"UNKNOWN_DEVICE",
//    	"macNo":"22:AA:AA:SD:DD",
//    	"deviceNo":"22:AA:AA:SD:DD",
//    	"platform":1,
//    	"weight":57.0,
//    	"resistance":334,
//    	"fat":27.4,
//    	"timestamp":1528805632
//    	},{
//    	"userId":3,
//    	"factoryId":1,
//    	"deviceName":"UNKNOWN_DEVICE",
//    	"macNo":"22:AA:AA:SD:DD",
//    	"deviceNo":"22:AA:AA:SD:DD",
//    	"platform":1,
//    	"weight":57.0,
//    	"resistance":334,
//    	"fat":27.4,
//    	"timestamp":1528805632
//    	},{
//    	"userId":4,
//    	"factoryId":1,
//    	"deviceName":"UNKNOWN_DEVICE",
//    	"macNo":"22:AA:AA:SD:DD",
//    	"deviceNo":"22:AA:AA:SD:DD",
//    	"platform":1,
//    	"weight":57.0,
//    	"resistance":334,
//    	"fat":27.4,
//    	"timestamp":1528805632
//    	}
//    	]
    

    @LoginPermission
    @PostMapping("save-batch.d")
    @ApiOperation(value="批量数据保持", response=FvWeightinfoEntity.class)
    public Result saveBatch(@RequestBody String json){  
    	if(StringUtils.isEmpty(json)) { 
    		return Result.error("很抱歉是上传数据文空！");
    	}
    	List<FvWeightinfoEntity> list = JSON.parseArray(json, FvWeightinfoEntity.class); 
    	if(null==list||list.size()==0) {
    		return Result.ok();
    	}
    	for (FvWeightinfoEntity vo : list) { 
    	  vo.setCreateTime(DateUtils.timeStampToDate(vo.getTimestamp())); 
        }
        return Result.ok().put("data", fvWeightinfoService.insertBatch(list, list.size()));
    } 

}
