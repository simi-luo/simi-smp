package com.simi.smp.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.simi.core.annotation.LoginPermission;
import com.simi.core.annotation.LoginUser;
import com.simi.core.utils.Result;
import com.simi.smp.entity.SysUserEntity;
import com.simi.smp.form.LoginForm;
import com.simi.smp.form.RegisterForm;
import com.simi.smp.service.SysTokenService;
import com.simi.smp.service.SysUserService;

import springfox.documentation.annotations.ApiIgnore;
import java.util.Date;
import java.util.Map;

 
/**
* <b>Description:</b><br>
*  用户登录、退出、注册   Controller
*
* @author:  holele
* @version:  1.0
* @Note
* <b>ProjectName:</b> simi-smp-fvsvr
* <br><b>PackageName:</b> com.simi.smp.controller
* <br><b>ClassName:</b> ApiLoginController
* <br><b>Date:</b> 2018年6月13日 下午1:13:24
*/ 
@RestController 
@RequestMapping({"/ios/users","/android/users"})
@Api(tags="用户相关接口")
public class ApiUsersController {
  
    @Autowired
    private SysUserService userService;
    
    @Autowired
    private SysTokenService tokenService;


    /**   
    * <b>Description:</b><br>
    * 登录接口
    *
    * @MethodName: login  
    * @param form  
    * @return Result: 返回类型  
    */
    @PostMapping("login.d")
    @ApiOperation("登录")
    public Result login(@ApiIgnore String userName,@ApiIgnore String password){  
      LoginForm form  = new LoginForm();
      form.setUserName(userName);
      form.setPassword(password);
       Map<String, Object> map = userService.login(form);

       return Result.ok(map);
  }
 
    /**   
    * <b>Description:</b><br>
    * 统一退出接口
    *
    * @MethodName: logout  
    * @param userId：用户ID  
    * @return Result: 返回类型  
    */
    @LoginPermission
    @PostMapping("logout.d")
    @ApiOperation("退出")
    public Result logout(@ApiIgnore long userId){
        tokenService.expireToken(userId);
        return Result.ok();
    }
    
    
    /**   
    * <b>Description:</b><br>
    * 注册接口
    *
    * @MethodName: register  
    * @param form  
    * @return Result: 返回类型  
    */
    @PostMapping("register.d")
    @ApiOperation("注册")
    public Result register(@RequestBody RegisterForm form){ 
        SysUserEntity user = new SysUserEntity();
        user.setMobile(form.getMobile());
        user.setUsername(form.getUserName());
        user.setPassword(DigestUtils.sha256Hex(form.getPassword()));
        user.setCreateTime(new Date());
        userService.insert(user); 
        return Result.ok();
    }

    

    /**   
    * <b>Description:</b><br>
    * 测试用户登录
    *
    * @MethodName: testUserInfo  
    * @param user
    * @return: 说明 
    * @return Result: 返回类型  
    */
    @LoginPermission
    @GetMapping("test-usersinfo.json")
    @ApiOperation(value="获取用户信息", response=SysUserEntity.class)
    public Result testUserInfo(@ApiIgnore @LoginUser SysUserEntity user){
        return Result.ok().put("user", user);
    }

    /**   
    * <b>Description:</b><br>
    * 测试获取用户
    *
    * @MethodName: testUserInfo  
    * @param userId
    * @return: 说明 
    * @return Result: 返回类型  
    */
    @LoginPermission
    @GetMapping("test-usersinfo-byid.json")
    @ApiOperation("获取用户ID")
    public Result testUserInfo(@ApiIgnore @RequestAttribute("userId") Integer userId){
        return Result.ok().put("userId", userId);
    }
    

}
