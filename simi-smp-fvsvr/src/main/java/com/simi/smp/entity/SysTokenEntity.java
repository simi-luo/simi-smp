/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.simi.smp.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;
 
/**
* <b>Description:</b><br>
*  系统登录Token存储
*
* @author:  holele
* @version:  1.0
* @Note
* <b>ProjectName:</b> simi-smp-fvsvr
* <br><b>PackageName:</b> com.simi.smp.entity
* <br><b>ClassName:</b> SysTokenEntity
* <br><b>Date:</b> 2018年6月13日 下午1:29:47
*/ 
@TableName("sys_token")
public class SysTokenEntity implements Serializable {
	private static final long serialVersionUID = 1L;
 
	/** @Fields userId : 用户ID   */  
	@TableId(type=IdType.INPUT)
	private Long userId;
	
	/** @Fields token : 登录Token  */  
	private String token;
	
	 
	/** @Fields expireTime : 登录过期时间   */  
	private Date expireTime; 
	
	/** @Fields updateTime : 登录更新时间   */  
	private Date updateTime;
 
	/**   
	* <b>Description:</b><br>
	* 设置：用户ID
	*
	* @MethodName: setUserId  
	* @param userId: 用户ID 
	* @return void: 返回类型  
	*/
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	 
	/**   
	* <b>Description:</b><br>
	* 获取：用户ID
	*
	* @MethodName: getUserId    
	* @return Long: 返回类型  
	*/
	public Long getUserId() {
		return userId;
	}

	/**   
	* <b>Description:</b><br>
	* 设置：token
	*
	* @MethodName: setToken  
	* @param token: token值 
	* @return void: 返回类型  
	*/
	public void setToken(String token) {
		this.token = token;
	} 
	/**   
	* <b>Description:</b><br>
	* 获取：token
	*
	* @MethodName: getToken    
	* @return String: 返回类型  
	*/
	public String getToken() {
		return token;
	}
	 
	/**   
	* <b>Description:</b><br>
	* 设置：过期时间
	*
	* @MethodName: setExpireTime  
	* @param expireTime: 说明 
	* @return void: 返回类型  
	*/
	public void setExpireTime(Date expireTime) {
		this.expireTime = expireTime;
	}
	 
	/**   
	* <b>Description:</b><br>
	*  获取：过期时间
	*
	* @MethodName: getExpireTime  
	* @return: 说明 
	* @return Date: 返回类型  
	*/
	public Date getExpireTime() {
		return expireTime;
	}
 
	/**   
	* <b>Description:</b><br>
	* 设置：更新时间
	*
	* @MethodName: setUpdateTime  
	* @param updateTime: 说明 
	* @return void: 返回类型  
	*/
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	} 
	/**   
	* <b>Description:</b><br>
	* 获取：更新时间
	*
	* @MethodName: getUpdateTime  
	* @return: 说明 
	* @return Date: 返回类型  
	*/
	public Date getUpdateTime() {
		return updateTime;
	}
}
