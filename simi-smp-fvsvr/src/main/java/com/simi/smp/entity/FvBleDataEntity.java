package com.simi.smp.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;


@TableName("fv_ble_data")
public class FvBleDataEntity implements Serializable {

  /** @Fields serialVersionUID :  用一句话描述这个变量表示什么   */  
  private static final long serialVersionUID = 123142153268966456L;

  /** @Fields id :   ID  */  
  @TableId(type = IdType.INPUT)
  private int id;  
  /** @Fields name :  协议名称   */  
  private String name;
  /** @Fields data :  协议内容，多条用";"号分开   */  
  private String data;
  
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  public String getData() {
    return data;
  }
  public void setData(String data) {
    this.data = data;
  }
  
  @Override
  public String toString() {
    return "FvBleDataEntity [id=" + id + ", name=" + name + ", data=" + data + "]";
  }
  
  
}
