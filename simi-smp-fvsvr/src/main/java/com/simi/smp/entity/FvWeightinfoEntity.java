package com.simi.smp.entity;

import java.io.Serializable;
import java.util.Date;
import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.simi.core.utils.DateUtils; 

@TableName("fv_weightinfo")
public class FvWeightinfoEntity implements Serializable {
	private static final long serialVersionUID = 1234312533254363232L; 
	
	/** @Fields id :  ID  */  
	@TableId(type = IdType.INPUT)
	private Long id; 
	/** @Fields userId :  用户ID  */  
	private int userId;
	/** @Fields factoryId :  工厂ID   */  
	private int factoryId;
	/** @Fields deviceName : 设备名称 */  
	private String deviceName;
	/** @Fields macNo :  Mac地址   */  
	private String macNo;
	/** @Fields deviceNo :  设备编号   */  
	private String deviceNo;
	/** @Fields platform : 平台类型   */  
	private int platform;
	/** @Fields weight :  体重*/  
	private float weight;
	/** @Fields resistance :  电阻   */  
	private int resistance;
	/** @Fields fat :  脂肪   */  
	private float fat;
	/** @Fields timestamp : 数据产生截止时间   */  
	private int timestamp;


    @JSONField(serialize=false)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getFactoryId() {
		return factoryId;
	}

	public void setFactoryId(int factoryId) {
		this.factoryId = factoryId;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getMacNo() {
		return macNo;
	}

	public void setMacNo(String macNo) {
		this.macNo = macNo;
	}

	public String getDeviceNo() {
		return deviceNo;
	}

	public void setDeviceNo(String deviceNo) {
		this.deviceNo = deviceNo;
	}

	public int getPlatform() {
		return platform;
	}

	public void setPlatform(int platform) {
		this.platform = platform;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public int getResistance() {
		return resistance;
	}

	public void setResistance(int resistance) {
		this.resistance = resistance;
	}

	public float getFat() {
		return fat;
	}

	public void setFat(float fat) {
		this.fat = fat;
	}

	public int getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(int timestamp) {
		this.timestamp = timestamp;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {    
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "FvWeightinfoEntity [id=" + id + ", userId=" + userId + ", factoryId=" + factoryId + ", deviceName="
				+ deviceName + ", macNo=" + macNo + ", deviceNo=" + deviceNo + ", platform=" + platform + ", weight="
				+ weight + ", resistance=" + resistance + ", fat=" + fat + ", timestamp=" + timestamp + ", createTime="
				+ createTime + "]";
	}

	
	
}
