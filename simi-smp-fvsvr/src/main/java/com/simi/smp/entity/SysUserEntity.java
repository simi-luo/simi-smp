/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.simi.smp.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;


 
/**
* <b>Description:</b><br>
*  系统登录用户
*
* @author:  holele
* @version:  1.0
* @Note
* <b>ProjectName:</b> simi-smp-fvsvr
* <br><b>PackageName:</b> com.simi.smp.entity
* <br><b>ClassName:</b> SysUserEntity
* <br><b>Date:</b> 2018年6月13日 下午1:36:00
*/
@TableName("sys_user")
public class SysUserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

 
	/** @Fields userId : 用户ID   */  
	@TableId
	private Long userId; 
	
	/** @Fields username : 用户名 */  
	private String username;
    /** @Fields factoryId : 工厂编号 */  
    private Integer factoryId;
	
	
 
	/** @Fields mobile : 手机号   */  
	private String mobile;
	 
	/** @Fields password : 密码   */  
	@JSONField(serialize=false)
	private String password;
	
	 
	/** @Fields createTime : 创建时间   */  
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

 
	/**   
	* <b>Description:</b><br>
	* 设置用户ID
	*
	* @MethodName: setUserId  
	* @param userId: 说明 
	* @return void: 返回类型  
	*/
	public void setUserId(Long userId) {
		this.userId = userId;
	}
 
	/**   
	* <b>Description:</b><br>
	*  获取用户ID
	*
	* @MethodName: getUserId  
	* @return: 说明 
	* @return Long: 返回类型  
	*/
	public Long getUserId() {
		return userId;
	} 
	/**   
	* <b>Description:</b><br>
	* 设置用户名
	*
	* @MethodName: setUsername  
	* @param username: 说明 
	* @return void: 返回类型  
	*/
	public void setUsername(String username) {
		this.username = username;
	}
	 
	/**   
	* <b>Description:</b><br>
	* 获取用户名
	*
	* @MethodName: getUsername  
	* @return: 说明 
	* @return String: 返回类型  
	*/
	public String getUsername() {
		return username;
	}
	 
	/**   
	* <b>Description:</b><br>
	*  设置手机号
	*
	* @MethodName: setMobile  
	* @param mobile: 说明 
	* @return void: 返回类型  
	*/
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	 
	/**   
	* <b>Description:</b><br>
	* 获取手机号
	*
	* @MethodName: getMobile  
	* @return: 说明 
	* @return String: 返回类型  
	*/
	public String getMobile() {
		return mobile;
	}
 
	/**   
	* <b>Description:</b><br>
	* 设置密码
	*
	* @MethodName: setPassword  
	* @param password: 说明 
	* @return void: 返回类型  
	*/
	public void setPassword(String password) {
		this.password = password;
	}
	 
	/**   
	* <b>Description:</b><br>
	* 获取密码
	*
	* @MethodName: getPassword  
	* @return: 说明 
	* @return String: 返回类型  
	*/
	public String getPassword() {
		return password;
	}
 
	/**   
	* <b>Description:</b><br>
	*  设置创建时间
	*
	* @MethodName: setCreateTime  
	* @param createTime: 说明 
	* @return void: 返回类型  
	*/
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	 
	/**   
	* <b>Description:</b><br>
	* 获取创建时间
	*
	* @MethodName: getCreateTime  
	* @return: 说明 
	* @return Date: 返回类型  
	*/
	public Date getCreateTime() {
		return createTime;
	}

    /**   
    * <b>Description:</b><br>
    * 获取工厂ID
    *
    * @MethodName: getFactoryId  
    * @return: 说明 
    * @return int: 返回类型  
    */
    public Integer getFactoryId() {
      return factoryId;
    }
  
    /**   
    * <b>Description:</b><br>
    * 设置工厂ID
    *
    * @MethodName: setFactoryId  
    * @param factoryId: 说明 
    * @return void: 返回类型  
    */
    public void setFactoryId(Integer factoryId) {
      this.factoryId = factoryId;
    }
	
	
}
