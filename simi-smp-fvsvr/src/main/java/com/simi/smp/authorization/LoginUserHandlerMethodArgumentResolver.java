/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.simi.smp.authorization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.simi.core.annotation.LoginUser;
import com.simi.smp.authorization.AuthorizationInterceptor;
import com.simi.smp.entity.SysUserEntity;
import com.simi.smp.service.SysUserService;

 
/**
* <b>Description:</b><br>
*  注解的方法参数，注入当前登录用户
*
* @author:  holele
* @version:  1.0
* @Note
* <b>ProjectName:</b> simi-smp-fvsvr
* <br><b>PackageName:</b> com.simi.smp.authorization
* <br><b>ClassName:</b> LoginUserHandlerMethodArgumentResolver
* <br><b>Date:</b> 2018年6月13日 下午1:05:04
*/
 
@Component
public class LoginUserHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {
	
	
    @Autowired
    private SysUserService userService;

    /**
    * <b>Description: </b><br>
    * 描述
    * @MethodName: supportsParameter
    * @param parameter
    * @return  
    * @see org.springframework.web.method.support.HandlerMethodArgumentResolver#supportsParameter(org.springframework.core.MethodParameter)    
     */
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterType().isAssignableFrom(SysUserEntity.class) && parameter.hasParameterAnnotation(LoginUser.class);
    }

    /**
    * <b>Description: </b><br>
    * 描述
    * @MethodName: resolveArgument
    * @param parameter：Method 参数
    * @param container：container 参数
    * @param request：请求参数
    * @param factory：WebDataBinder 参数
    * @return
    * @throws Exception  
    * @see org.springframework.web.method.support.HandlerMethodArgumentResolver#resolveArgument(org.springframework.core.MethodParameter, org.springframework.web.method.support.ModelAndViewContainer, org.springframework.web.context.request.NativeWebRequest, org.springframework.web.bind.support.WebDataBinderFactory)    
     */
    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer container,
                                  NativeWebRequest request, WebDataBinderFactory factory) throws Exception {
        Object object = request.getAttribute(AuthorizationInterceptor.USER_KEY, RequestAttributes.SCOPE_REQUEST);
        if(object == null){
            return null;
        }
        //获取用户信息
        SysUserEntity user = userService.selectById((Long)object);
        return user;
    }
}
