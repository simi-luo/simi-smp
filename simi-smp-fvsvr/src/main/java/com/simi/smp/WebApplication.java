package com.simi.smp;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


/**
* <b>Description:</b><br>
* Spring Boot Application
*
* @author:  holele
* @version:  1.0
* @Note
* <b>ProjectName:</b> simi-smp-fvsvr
* <br><b>PackageName:</b> com.simi.smp
* <br><b>ClassName:</b> WebApplication
* <br><b>Date:</b> 2018年6月13日 上午11:45:41
*/ 
@SpringBootApplication
@MapperScan(basePackages = {"com.simi.smp.mapper"})
public class WebApplication extends SpringBootServletInitializer {

	/**   
	* <b>Description:</b><br>
	* 确定main函数
	*
	* @MethodName: main  
	* @param args: 说明 
	* @return void: 返回类型  
	*/
	public static void main(String[] args) {
		SpringApplication.run(WebApplication.class, args);
	}

	/**
	* <b>Description: </b><br>
	* 描述
	* @MethodName: configure
	* @param application
	* @return  
	* @see org.springframework.boot.web.servlet.support.SpringBootServletInitializer#configure(org.springframework.boot.builder.SpringApplicationBuilder)    
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(WebApplication.class);
	}
}
