package com.simi.core.exception;

/**
* <b>Description:</b><br>
*  描述内容
*
* @author:  lhj58
* @version:  1.0
* @Note
* <b>ProjectName:</b> simi-smp-core
* <br><b>PackageName:</b> com.simi.core.exception
* <br><b>ClassName:</b> SIMIException
* <br><b>Date:</b> 2018年6月2日 上午12:51:35
*/
 
public class SimiException extends RuntimeException {
  private static final long serialVersionUID = 1L;

  private String msg;
  private int code = 500;

  public SimiException(String msg) {
    super(msg);
    this.msg = msg;
  }

  public SimiException(String msg, Throwable e) {
    super(msg, e);
    this.msg = msg;
  }

  public SimiException(String msg, int code) {
    super(msg);
    this.msg = msg;
    this.code = code;
  }

  public SimiException(String msg, int code, Throwable e) {
    super(msg, e);
    this.msg = msg;
    this.code = code;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }


}
