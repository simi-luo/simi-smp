package com.simi.core.enums;

/**
* <b>Description:</b><br>
*  时间 格式枚举值
*
* @author:  holele
* @version:  1.0
* @Note
* <b>ProjectName:</b> simi-smp-core
* <br><b>PackageName:</b> com.simi.core.enums
* <br><b>ClassName:</b> EnumDatePattern
* <br><b>Date:</b> 2018年6月1日 下午2:13:52
*/
 
public enum EnumDatePattern {

  /** @Fields DATE_PATTERN_STR :  时间格式(yyyy-MM-dd) */  
  DATE_PATTERN_STR(1,"yyyy-MM-dd"),
  /** @Fields DATE_PATTERN_STR1 :  时间格式(yyyy/MM/dd) */  
  DATE_PATTERN_STR1(2,"yyyy/MM/dd"),
  /** @Fields DATE_PATTERN_STR2 :  时间格式(MM/dd/yy) */  
  DATE_PATTERN_STR2(3,"MM/dd/yy"),
  /** @Fields DATE_PATTERN_STR3 :  时间格式(yyyy年MM月dd日) */  
  DATE_PATTERN_STR3(4,"yyyy年MM月dd日"),
  /** @Fields DATE_PATTERN_STR4 :  时间格式(MM月dd日yy年) */  
  DATE_PATTERN_STR4(5,"MM月dd日yy年"),
  /** @Fields DATE_PATTERN_NUM : 时间格式(yyyyMMdd)   */  
  DATE_PATTERN_NUM(6,"yyyyMMdd"),
  /** @Fields DATE_TIME_PATTERN_STR :  时间格式(yyyy-MM-dd HH:mm:ss)  */  
  DATE_TIME_PATTERN_STR(7,"yyyy-MM-dd HH:mm:ss"),
  /** @Fields DATE_TIME_PATTERN_STR1 :  时间格式(yyyy-MM-dd HH:mm:ss:SSS)  */  
  DATE_TIME_PATTERN_STR1(7,"yyyy-MM-dd HH:mm:ss:SSS"),
  /** @Fields DATE_TIME_PATTERN_STR2 :  时间格式(yyyy/MM/dd HH:mm:ss)  */  
  DATE_TIME_PATTERN_STR2(8,"yyyy/MM/dd HH:mm:ss"),
  /** @Fields DATE_TIME_PATTERN_STR3 :  时间格式(yyyy/MM/dd HH:mm:ss:SSS)  */  
  DATE_TIME_PATTERN_STR3(9,"yyyy/MM/dd HH:mm:ss:SSS"),
  /** @Fields DATE_TIME_PATTERN_NUM :  时间格式(yyyyMMddHHmmss)  */  
  DATE_TIME_PATTERN_NUM(10,"yyyyMMddHHmmss"),
  /** @Fields DATE_TIME_PATTERN_NUM2 :  时间格式(yyyyMMddHHmmssSSS)  */  
  DATE_TIME_PATTERN_NUM2(11,"yyyyMMddHHmmssSSS"),
  /** @Fields DATE_TIME_PATTERN_NUM3 :  时间格式(yyMMddHHmmss)  */  
  DATE_TIME_PATTERN_NUM3(12,"yyMMddHHmmss"),
  /** @Fields DATE_TIME_PATTERN_NUM4 :  时间格式(yyMMddHHmmssSSS)  */  
  DATE_TIME_PATTERN_NUM4(13,"yyMMddHHmmssSSS"),
  /** @Fields TIME_PATTERN :  时间格式(HH:mm:ss)   */  
  TIME_PATTERN(14,"HH:mm:ss");
  
  
  /** @Fields val :  枚舉Val 值*/  
  private int val;
  /** @Fields name :  枚舉Name值 */  
  private String name ;
  
  /**   
  * <b>Description:</b><br> 
  * 枚举函数 
  *  
  * @param val: 枚舉Val 
  * @param name:  枚舉Name值 
  */
  EnumDatePattern(int val,String name){
    this.name = name;
    this.val = val;
  }
  
  
  
  /**   
  * <b>Description:</b><br>
  * 获取枚举Val值
  *
  * @MethodName: getVal    
  * @return int: 返回获取到的Val值 
  */
  public int getVal() {
    return val;
  }



  /**   
  * <b>Description:</b><br>
  * 设置枚举Val值
  *
  * @MethodName: setVal  
  * @param val: 设置的Val值 
  * @return void: 返回类型  
  */
  public void setVal(int val) {
    this.val = val;
  }



  /**   
  * <b>Description:</b><br>
  * 获取枚举Name值
  *
  * @MethodName: getName   
  * @return String: 枚举Name值 
  */
  public String getName() {
    return name;
  }



  /**   
  * <b>Description:</b><br>
  * 设置枚举Name值
  *
  * @MethodName: setName  
  * @param name: name值 
  * @return void: 返回类型  
  */
  public void setName(String name) {
    this.name = name;
  }



  /**   
  * <b>Description:</b><br>
  * 根据枚举Val 获取枚举中Name值
  *
  * @MethodName: getName  
  * @param val
  * @return: 说明 
  * @return String: 返回类型  
  */
  public static String getName(int val) {
    for (EnumDatePattern p :EnumDatePattern.values()) {
      if(p.getVal() == val) {
        return p.getName();
      }
    }
    return "";
  }
  
  
}
