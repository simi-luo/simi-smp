package com.simi.core.enums;

/**
 * <b>Description:</b><br>
 * 添加日期类型枚举类
 *
 * @author: holele
 * @version: 1.0
 * @Note <b>ProjectName:</b> simi-smp-core <br>
 *       <b>PackageName:</b> com.simi.core.enums <br>
 *       <b>ClassName:</b> EnumAddDateType <br>
 *       <b>Date:</b> 2018年6月1日 下午1:54:04
 */
public enum EnumDateType {
  /** @Fields SECONDS : 秒数，负数为减 加/减几秒后的日期 */
  SECONDS,
  /** @Fields MINUTES : 分钟数，负数为减 加/减几分钟后的日期 */
  MINUTES,
  /** @Fields HOURS : 小时数，负数为减 日期 加/减几小时后的日期 */
  HOURS,
  /** @Fields DAYS : 天数，负数为减 日期 加/减几天后的日期 */
  DAYS,
  /** @Fields WEEKS : 周数，负数为减 日期 加/减几周后的日期 */
  WEEKS,
  /** @Fields MONTHS : 月数 加/减几月后的日期 */
  MONTHS,
  /** @Fields YEARS :年数 加/减几年后的日期 */
  YEARS;
}
