package com.simi.core.enums;

/**
* <b>Description:</b><br>
*  枚举系统Key规范
*
* @author:  lhj58
* @version:  1.0
* @Note
* <b>ProjectName:</b> simi-smp-core
* <br><b>PackageName:</b> com.simi.core.enums
* <br><b>ClassName:</b> EnumSysKey
* <br><b>Date:</b> 2018年6月2日 上午12:44:59
*/ 
public enum EnumSysKey {
  
  /** @Fields SYS_ADMIN :   ADMIN系统key   */  
  SYS_ADMIN(1,"sys:"),
  /** @Fields SYS_API :  API系统key   */  
  SYS_API(2,"api:"),
  /** @Fields SYS_KPI :  KPI系统key  */  
  SYS_KPI(3,"kip:"),
  /** @Fields SYS_OA :  oa系统key*/  
  SYS_OA(4,"oa:");
  
  /** @Fields val :  val 值   */  
  private int val;
  /** @Fields name : name 值   */  
  private String name;
  
  /**   
  * <b>Description:</b><br> 
  * 枚举构造 
  * @author lhj58 
  * @param val：枚举val值
  * @param name: 枚举name值 
  */
  EnumSysKey(int val,String name){
    this.val = val;
    this.name = name;
  }
  
  /**   
  * <b>Description:</b><br>
  * 获取Val值
  *
  * @MethodName: getVal    
  * @return int: 返回类型  
  */
  public int getVal() {
    return val;
  }
  /**   
  * <b>Description:</b><br>
  * 设置Val 值
  *
  * @MethodName: setVal  
  * @param val: val值 
  * @return void: 返回类型  
  */
  public void setVal(int val) {
    this.val = val;
  }
  /**   
  * <b>Description:</b><br>
  * 获取枚举Name值
  *
  * @MethodName: getName    
  * @return String: 返回类型  
  */
  public String getName() {
    return name;
  }
  
  /**   
  * <b>Description:</b><br>
  * 设置Name值
  *
  * @MethodName: setName  
  * @param name: 枚举name值 
  * @return void: 返回类型  
  */
  public void setName(String name) {
    this.name = name;
  }
  
  /**   
  * <b>Description:</b><br>
  * 获取Name值
  *
  * @MethodName: getName  
  * @param val :枚举Val 值
  * @return String: 返回类型  
  */
  public String getName(int val) {
    for (EnumSysKey c :EnumSysKey.values()) {
      if(c.val == val ) {
        return c.name;
      }
    }
    return "";
  }

  /**   
  * <b>Description:</b><br>
  * 获取name 值
  *
  * @MethodName: getName  
  * @param val :枚举值
  * @return String: 返回类型  
  */
  public String getName(EnumSysKey val) {
    for (EnumSysKey c :EnumSysKey.values()) {
      if(c == val ) {
        return c.name;
      }
    }
    return "";
  }

}
