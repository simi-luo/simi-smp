package com.simi.core.utils;

import com.alibaba.fastjson.JSON;

/**
* <b>Description:</b><br>
*  ClassUtils 工具类
*
* @author:  holele
* @version:  1.0
* @Note
* <b>ProjectName:</b> simi-smp-core
* <br><b>PackageName:</b> com.simi.core.utils
* <br><b>ClassName:</b> ClassUtils
* <br><b>Date:</b> 2018年6月2日 下午5:50:19
*/
 
public class ClassUtils {
  /**   
   * <b>Description:</b><br>
   * 把JSON数据，转成Object
   *
   * @MethodName: fromJson  
   * @param json: json字符串
   * @param clazz:泛型类对象  
   * @return T: 返回T泛型类型 
   */
   public static <T> T fromJson(String json, Class<T> clazz) {
     return JSON.parseObject(json, clazz);
   }
}
