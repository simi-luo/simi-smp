package com.simi.core.utils;

import java.io.Serializable;
import java.util.List;
import com.baomidou.mybatisplus.plugins.Page;

/**
* <b>Description:</b><br>
*  分页工具类
*
* @author:  holele
* @version:  1.0
* @Note
* <b>ProjectName:</b> simi-smp-core
* <br><b>PackageName:</b> com.simi.core.utils
* <br><b>ClassName:</b> PageUtils
* <br><b>Date:</b> 2018年6月2日 下午5:50:58
*/
 
public class PageUtils implements Serializable {
  private static final long serialVersionUID = 1L;
  // 总记录数
  /** @Fields totalCount : 总记录数   */  
  private int totalCount; 
  /** @Fields pageSize :  每页记录数   */  
  private int pageSize; 
  /** @Fields totalPage : 总页数   */  
  private int totalPage; 
  /** @Fields currPage : 当前页数   */  
  private int currPage; 
  /** @Fields list :  列表数据  */  
  private List<?> list;

  /**
   * 分页
   * 
   * @param list 列表数据
   * @param totalCount 总记录数
   * @param pageSize 每页记录数
   * @param currPage 当前页数
   */
  public PageUtils(List<?> list, int totalCount, int pageSize, int currPage) {
    this.list = list;
    this.totalCount = totalCount;
    this.pageSize = pageSize;
    this.currPage = currPage;
    this.totalPage = (int) Math.ceil((double) totalCount / pageSize);
  }

  /**
   * 分页
   */
  public PageUtils(Page<?> page) {
    this.list = page.getRecords();
    this.totalCount = page.getTotal();
    this.pageSize = page.getSize();
    this.currPage = page.getCurrent();
    this.totalPage = page.getPages();
  }

  public int getTotalCount() {
    return totalCount;
  }

  public void setTotalCount(int totalCount) {
    this.totalCount = totalCount;
  }

  public int getPageSize() {
    return pageSize;
  }

  public void setPageSize(int pageSize) {
    this.pageSize = pageSize;
  }

  public int getTotalPage() {
    return totalPage;
  }

  public void setTotalPage(int totalPage) {
    this.totalPage = totalPage;
  }

  public int getCurrPage() {
    return currPage;
  }

  public void setCurrPage(int currPage) {
    this.currPage = currPage;
  }

  public List<?> getList() {
    return list;
  }

  public void setList(List<?> list) {
    this.list = list;
  }

}
