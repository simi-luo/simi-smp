package com.simi.core.utils;

import java.io.UnsupportedEncodingException; 
import java.util.Date;
import java.util.Random;
import java.util.UUID;
import com.simi.core.enums.EnumDatePattern;

/**
 * <b>Description:</b><br>
 * 常用编码、验证码生产工具类
 *
 * @author: holele
 * @version: 1.0
 * @Note <b>ProjectName:</b> simi-smp-core <br>
 *       <b>PackageName:</b> com.simi.core.utils <br>
 *       <b>ClassName:</b> CodeUtils <br>
 *       <b>Date:</b> 2018年6月2日 下午6:36:15
 */

public class CodeUtils {


  /** @Fields LEN_DEFULT : 默认长度 */
  private static int LEN_DEFULT = 20;
  /** @Fields LEN_MIN : 最小长度 */
  private static int LEN_MIN = 18;
  /** @Fields LEN_MAX : 最多长度 */
  private static int LEN_MAX = 25;
  /** @Fields LENCODE : 最多长度 */
  private static int LEN_CODE = 6;

  /**
   * <b>Description:</b><br>
   * 生产很时间修改的编号： 使用场景：订单号修改
   *
   * @MethodName: num
   * @param len：长度
   * @return String: 返回类型 格式： yyyyMMddHHmmssSSS000000
   */
  public static String num(int len) {
    if (len <= LEN_MIN) {
      len = LEN_MIN;
    }
    if (len >= LEN_MAX) {
      len = LEN_MAX;
    }
    String dateNum = DateUtils.format(new Date(), EnumDatePattern.DATE_TIME_PATTERN_NUM2);
    long code = Math.round(Math.random() * 1000000000);
    return new StringBuffer().append(dateNum).append(String.valueOf(code)).toString().substring(0,
        len);
  }

  /**
   * <b>Description:</b><br>
   * 生产很时间修改的编号： 默认编号长度20位数字字符串 使用场景：订单号修改
   *
   * @MethodName: num
   * @return String: 返回类型 格式： yyyyMMddHHmmssSSS000000
   */
  public static String num() {
    String dateNum = DateUtils.format(new Date(), EnumDatePattern.DATE_TIME_PATTERN_NUM2);
    long code = Math.round(Math.random() * 1000000000);
    return new StringBuffer().append(dateNum).append(String.valueOf(code)).toString().substring(0,
        LEN_DEFULT);
  }

  /**
   * <b>Description:</b><br>
   * 生产数据在 min-max 之间的数字
   *
   * @MethodName: numMinToMax
   * @param min
   * @param max
   * @return: 说明
   * @return Integer: 返回类型
   */
  public static Integer numMinToMax(final int min, final int max) {
    Random rand = new Random();
    int tmp = Math.abs(rand.nextInt());
    return tmp % (max - min + 1) + min;
  }



  /**
   * <b>Description:</b><br>
   * 生产指6位默认长度的数据随机验证码
   *
   * @MethodName: validateCode
   * @param len
   * @return: 说明
   * @return int: 返回类型
   */
  public static int validateCode() {
    return validateCode(LEN_CODE);
  }

  /**
   * <b>Description:</b><br>
   * 生产指定长度的数据随机验证码
   *
   * @MethodName: validateCode
   * @param len
   * @return: 说明
   * @return int: 返回类型
   */
  public static int validateCode(int len) {
    int min = Integer.valueOf(String.valueOf(Math.pow(10, len - 1)));
    int max = Integer.valueOf(String.valueOf(Math.pow(10, len)));
    return numMinToMax(min, max - 1);
  }


  /**
   * <b>Description:</b><br>
   * 生产包含数据和字母的验证码【字母是不区分大小写】
   *
   * @MethodName: validateCodeCase
   * @param len：指定生成长度
   * @return String: 返回类型
   */
  public static String validateCodeCase(int len) {
    String val = "";
    Random random = new Random();
    // length为几位密码
    for (int i = 0; i < len; i++) {
      String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num";
      // 输出字母还是数字
      if ("char".equalsIgnoreCase(charOrNum)) {
        // 输出是大写字母还是小写字母
        int temp = random.nextInt(2) % 2 == 0 ? 65 : 97;
        val += (char) (random.nextInt(26) + temp);
      } else if ("num".equalsIgnoreCase(charOrNum)) {
        val += String.valueOf(random.nextInt(10));
      }
    }
    return val;
  }



  /**
   * <b>Description:</b><br>
   * 生产包含数据和字母的验证码 【字母是大写】
   *
   * @MethodName: validateCodeCase
   * @param len：指定生成长度
   * @return String: 返回类型
   */
  public static String validateCodeUpperCase(int len) {
    return validateCodeCase(len).toUpperCase();
  }

  /**
   * <b>Description:</b><br>
   * 生产包含数据和字母的验证码【字母是小写】
   *
   * @MethodName: validateCodeCase
   * @param len：指定生成长度
   * @return String: 返回类型
   */
  public static String validateCodeLowerCase(int len) {
    return validateCodeCase(len).toLowerCase();
  }


  /**   
  * <b>Description:</b><br>
  * 生产中文数据验证汉字
  *
  * @MethodName: validateCodeChinese  
  * @param len :指定長度 
  * @return String: 返回类型  
  */
  @SuppressWarnings("deprecation")
  public static String validateCodeChinese(int len) {
    String ret = "";
    String charsetName = "GBk";
    int num = 39;
    for (int i = 0; i < len; i++) {
      String str = null;
      int hightPos, lowPos; 
      // 定义高低位
      Random random = new Random();
      hightPos = (176 + Math.abs(random.nextInt(num))); 
      lowPos = (161 + Math.abs(random.nextInt(num)));  
      byte[] b = new byte[2]; 
      b[0] = (new Integer(hightPos).byteValue());
      b[1] = (new Integer(lowPos).byteValue());
      try {
      // 转成中文
        str = new String(b, charsetName); 
      } catch (UnsupportedEncodingException ex) {
        ex.printStackTrace();
      }
      ret += str;
    }
    return ret;
  }
  
  /**   
  * <b>Description:</b><br>
  * 获取Token值
  *
  * @MethodName: getToken  
  * @return: 说明 
  * @return String: 返回类型  
  */
  public static String getToken(){
    return UUID.randomUUID().toString().replace("-", "");
}

  public static void main(String[] args) {
    System.out.println(validateCodeCase(8));
    System.out.println(validateCodeUpperCase(8));
    System.out.println(validateCodeLowerCase(8));
    System.out.println(validateCodeChinese(4));
  }

}
