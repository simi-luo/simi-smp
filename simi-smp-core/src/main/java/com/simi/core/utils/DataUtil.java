package com.simi.core.utils;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat; 
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

 
/**
* <b>Description:</b><br>
*  数据类型转换 工具类
*
* @author:  holele
* @version:  1.0
* @Note
* <b>ProjectName:</b> simi-smp-core
* <br><b>PackageName:</b> com.simi.core.utils
* <br><b>ClassName:</b> DataUtil
* <br><b>Date:</b> 2018年6月2日 下午6:20:20
*/
 
public class DataUtil {

    private static final Logger logger = LoggerFactory.getLogger(DataUtil.class);

     
   
    /**   
    * <b>Description:</b><br>
    * 描述：小数点保留两位
    *
    * @MethodName: convert  
    * @param value
    * @return: 说明 
    * @return double: 返回类型  
    */
    public static double convert(double value) {
        return convert(value, 2);
    }

 
    /**   
    * <b>Description:</b><br>
    * 小数点保留两位
    *
    * @MethodName: convert  
    * @param value:要转换的值
    * @param len   :要保留位数
    * @return double: 返回类型  
    */
    public static double convert(double value, int len) {
        long l1 = Math.round(value * Math.pow(10, len));  
        double ret = l1 / Math.pow(10, len);  
        return ret;
    }
 
    /**   
    * <b>Description:</b><br>
    * 保留多少文小数 ：使用四舍五入
    * float value:要转换的值
    * int len:要保留位数
    *
    * @MethodName: toFloat  
    * @param value:要转换的值
    * @param len  :要保留位数
    * @return float: 返回类型  
    */
    public static float toFloat(float value, int len) {
        if (value == 0 || value == 0.0) {
            return 0;
        } else { 
            return new BigDecimal(value).setScale(len, RoundingMode.UP).floatValue();

        }
    }

    /**   
    * <b>Description:</b><br>
    * 小时点格式函数
    *
    * @MethodName: decFormat  
    * @param len ：小数点多少位 
    * @return String: 返回类型  
    */
    private static String decFormat(int len) {
        String format1= ".0";
        String format2= ".00";
        String format3= ".000";
        String format4= ".0000"; 
        int temp1 = 1; 
        int temp2 = 2; 
        int temp3 = 3; 
        int temp4 = 4;
        
        String temp = ".0";
        if (temp1== len) {
            temp = format1;
        }
        if (temp2== len) {
            temp = format2;
        }
        if (temp3 == len) {
            temp = format3;
        }
        if (temp4 == len) {
            temp = format4;
        }
        return temp;
    }
 
    /**   
    * <b>Description:</b><br>
     * int  to  float ：使用四舍五入 
    *
    * @MethodName: intToFloat  
    * @param value:要转换的值
    * @param len:要保留位数  
    * @return float: 返回类型  
    */
    public static float intToFloat(int value, int len) {
        if (value == 0) {
            return 0.0f;
        } 
        return new BigDecimal(value).setScale(len, RoundingMode.UP).floatValue();  
    }

    /**   
    * <b>Description:</b><br>
     * float  to  float ：使用四舍五入 
    *
    *
    * @MethodName: floatToFloat  
    * @param value:要转换的值
    * @param len:要保留位数  
    * @return float: 返回类型  
    */
    public static float floatToFloat(float value, int len) {
        if (value == 0) {
            return 0.0f;
        }  
        return new BigDecimal(value).setScale(len, RoundingMode.UP).floatValue();  
    }

   
    /**   
    * <b>Description:</b><br>
     * 保留多少文小数 ：使用四舍五入 
    *
    * @MethodName: floatToStr  
    * @param value:要转换的值
    * @param len:要保留位数  
    * @return String: 返回类型  
    */
    public static String floatToStr(int value, int len) {
        if (value == 0) {
            return "0";
        } 
        return  new DecimalFormat(decFormat(len)).format(value); 
    }
 
    /**   
    * <b>Description:</b><br>
     * float to int 小数转换成整数 ：：使用四舍五入 
    *
    * @MethodName: toInt  
    * @param value :要转换的值 
    * @return int: 返回类型  
    */
    public static int toInt(float value) {
        if (value == 0) {
            return 0;
        } 
        return new BigDecimal(value).setScale(0, RoundingMode.UP).intValue();
    }
 
    /**   
    * <b>Description:</b><br>
     * long to Int long转换成整数 ：：使用四舍五入 
    *
    * @MethodName: toInt  
    * @param value:要转换的值  
    * @return int: 返回类型  
    */
    public static int toInt(Long value) {
        if (value == null || value == 0) {
            return 0;
        } 
        return  new BigDecimal(value).setScale(0, RoundingMode.UP).intValue();
    }
 
    /**   
    * <b>Description:</b><br>
    * float to long  小时传化long 
    *
    * @MethodName: toLong  
    * @param value  :要转换的值
    * @return Long: 返回类型  
    */
    public static Long toLong(float value) {
        if (value == 0) {
            return 0L;
        } 
        return new BigDecimal(value).setScale(0, RoundingMode.UP).longValue();
    }
 
//
//    public static void main(String[] args) throws ParseException, Exception {
//        System.out.println(convert(0.927497, 3)); 
//    }
}
