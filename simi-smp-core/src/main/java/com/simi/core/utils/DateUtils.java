package com.simi.core.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import com.simi.core.enums.EnumDatePattern;
import com.simi.core.enums.EnumDateType;

/**
 * <b>Description:</b><br>
 * 日期时间工具类
 *
 * @author: holele
 * @version: 1.0
 * @Note <b>ProjectName:</b> simi-smp-core <br>
 *       <b>PackageName:</b> com.simi.core.utils <br>
 *       <b>ClassName:</b> DateUtils <br>
 *       <b>Date:</b> 2018年6月1日 下午3:36:18
 */
public class DateUtils {

  
  /**
   *  <b>Description:</b><br>
   * 根据Unix时间戳，转换成Date
   *
   * @MethodName: timestamp
   * @return long: 时间戳
   */
  public static Date timeStampToDate(Integer timestamp) {
      Long value = timestamp * 1000l;
      return new java.util.Date(value);
  }
  /**
   * <b>Description:</b><br>
   * 获取当前时间时间戳
   *
   * @MethodName: timestamp
   * @return long: 时间戳
   */
  public static long timestamp() {
    return System.currentTimeMillis() / 1000;
  }


  /**
   * <b>Description:</b><br>
   * Date 转换 public
   *
   * @MethodName: getLocalDate
   * @param date:日期时间
   * @return LocalDate: 返回LocalDate时间值
   */
  public static LocalDate getLocalDate(Date date) {
    Instant instant = date.toInstant();
    ZoneId zoneId = ZoneId.systemDefault();
    // atZone()方法返回在指定时区从此Instant生成的ZonedDateTime。
    return instant.atZone(zoneId).toLocalDate();
  }


  /**
   * <b>Description:</b><br>
   * 获取制定时间的时间错误值
   *
   * @MethodName: timestamp
   * @param date：时间
   * @return long: 时间戳
   */
  public static long timestamp(Date date) {
    // System.currentTimeMillis()
    return date.getTime() / 1000;
  }

  /**
   * <b>Description:</b><br>
   * 连个时间比较先差值是多少
   *
   * @MethodName: differTwoDate
   * @param startDate:开始时间
   * @param endDate:截止时间
   * @param dateType:先差的类型，比较的类型
   * @return double: 返回先差值是多少
   */
  public static double differTwoDate(Date startDate, Date endDate, EnumDateType dateType) {
    if (null == startDate || null == endDate) {
      return 0;
    }
    long startDateValue = startDate.getTime();
    long endDateValue = endDate.getTime();
    System.out.println("startDateValue: " + startDateValue);
    System.out.println("  endDateValue: " + endDateValue);
    double result = 0;
    switch (dateType) {
      case SECONDS:
        /** 计算间隔多少SECONDS，则除以毫秒到天的转换公式 */
        result = (endDateValue - startDateValue) / (1000f);
        break;
      case MINUTES:
        // 计算间隔多少MINUTES，则除以毫秒到天的转换公式*/
        result = (endDateValue - startDateValue) / (1000 * 60f);
        break;
      case HOURS:
        // 计算间隔多少小时，则除以毫秒到天的转换公式*/
        result = (endDateValue - startDateValue) / (1000 * 60 * 60f);
        break;
      case DAYS:
        // 计算间隔多少天，则除以毫秒到天的转换公式*/
        result = (endDateValue - startDateValue) / (1000 * 60 * 60 * 24f);
        break;
      case WEEKS:
        // 计算间隔多少天，则除以毫秒到天的转换公式
        result = (endDateValue - startDateValue) / (1000 * 60 * 60 * 24f) / 7f;
        break;
      case MONTHS:
        // 计算间隔多少天，则除以毫秒到天的转换公式
        result = (endDateValue - startDateValue) / (1000 * 60 * 60 * 24f) / 30f;
        break;
      case YEARS:
        // 计算间隔多少天，则除以毫秒到天的转换公式
        result = (endDateValue - startDateValue) / (1000 * 60 * 60 * 24f) / 30f / 12f;
        break;
      default:
        break;
    }
    BigDecimal bg = new BigDecimal(result).setScale(2, RoundingMode.UP);
    return bg.doubleValue();
  }

  /**
   * <b>Description:</b><br>
   * 连个时间比较先差值是多少
   *
   * @MethodName: differTwoDate
   * @param startDate:开始时间
   * @param endDate:截止时间
   * @param dateType:先差的类型，比较的类型
   * @return String: 返回小时字符串
   */
  public static String differTwoDateStr(Date startDate, Date endDate, EnumDateType dateType) {
    return String.valueOf(differTwoDate(startDate, endDate, dateType));
  }


  /**
   * <b>Description:</b><br>
   * 连个时间比较先差值是多少
   *
   * @MethodName: differTwoDate
   * @param startDate:开始时间
   * @param endDate:截止时间
   * @param dateType:先差的类型，比较的类型
   * @return int: 返回int
   */
  public static int differTwoDateInt(Date startDate, Date endDate, EnumDateType dateType) {
    Double dg = differTwoDate(startDate, endDate, dateType);
    return dg.intValue();
  }


  /**
   * <b>Description:</b><br>
   * 日期格式化 日期格式为：yyyy-MM-dd
   *
   * @MethodName: format
   * @param date :日期
   * @return String: 返回yyyy-MM-dd格式日期
   */
  public static String format(Date date) {
    return format(date, EnumDatePattern.DATE_PATTERN_STR);
  }


  /**
   * <b>Description:</b><br>
   * 日期格式化 日期格式为：yyyy-MM-dd
   *
   * @MethodName: format
   * @param date
   * @param pattern： 格式，如：DateUtils.DATE_TIME_PATTERN
   * @return String: 返回yyyy-MM-dd格式日期
   */
  public static String format(Date date, EnumDatePattern pattern) {
    if (date != null) {
      SimpleDateFormat df = new SimpleDateFormat(pattern.getName());
      return df.format(date);
    }
    return null;
  }

  /**
   * <b>Description:</b><br>
   * 字符串转换成日期
   *
   * @MethodName: stringToDate
   * @param strDate： 日期字符串
   * @param pattern：日期的格式，如：yyyy-MM-dd HH:mm:ss:SSS
   * @return Date: 返回日期数据
   */
  public static Date stringToDate(String strDate, String pattern) {
    if (StringUtils.isBlank(strDate)) {
      return null;
    } 
    return DateTimeFormat.forPattern(pattern).parseLocalDateTime(strDate).toDate();
  }

  /**
   * <b>Description:</b><br>
   * 字符串转换成日期
   *
   * @MethodName: stringToDate
   * @param strDate： 日期字符串 时间格式(yyyy-MM-dd)
   * @return Date: 返回日期数据
   */
  public static Date stringToDate(String strDate) {
    return stringToDate(strDate, EnumDatePattern.DATE_PATTERN_STR);
  }

  /**
   * <b>Description:</b><br>
   * 字符串转换成日期
   *
   * @MethodName: stringToDate
   * @param strDate： 日期字符串 :时间格式(yyyy-MM-dd HH:mm:ss)
   * @return Date: 返回日期数据
   */
  public static Date stringToDateTime(String strDate) {
    return stringToDate(strDate, EnumDatePattern.DATE_TIME_PATTERN_STR);
  }

  /**
   * <b>Description:</b><br>
   * 字符串转换成日期
   *
   * @MethodName: stringToDate
   * @param strDate： 日期字符串
   * @param pattern：EnumDatePattern 日期的格式
   * @return Date: 返回日期数据
   */
  public static Date stringToDate(String strDate, EnumDatePattern pattern) {
    return stringToDate(strDate, pattern.getName());
  }


  /**
   * <b>Description:</b><br>
   * 对日期的【秒】进行加/减
   *
   * @MethodName: addDateSeconds
   * @param date: 日期
   * @param seconds: 秒数，负数为减
   * @return Date: 返回日期 加/减几秒后的日期
   */
  public static Date addDateSeconds(Date date, int seconds) {
    DateTime dateTime = new DateTime(date);
    return dateTime.plusSeconds(seconds).toDate();
  }

  /**
   * <b>Description:</b><br>
   * 对日期的【分钟】进行加/减
   *
   * @MethodName: addDateMinutes
   * @param date: 日期
   * @param minutes: 分钟数，负数为减
   * @return Date: 返回日期 加/减几分钟后的日期
   */
  public static Date addDateMinutes(Date date, int minutes) {
	DateTime dateTime = new DateTime(date);
    return dateTime.plusMinutes(minutes).toDate();
  }

  /**
   * <b>Description:</b><br>
   * 对日期的【小时】进行加/减
   *
   * @MethodName: addDateHours
   * @param date: 日期
   * @param hours:小时数，负数为减
   * @return Date: 返回日期 加/减几小时后的日期
   */
  public static Date addDateHours(Date date, int hours) {
	DateTime dateTime = new DateTime(date);
    return dateTime.plusHours(hours).toDate();
  }

  /**
   * <b>Description:</b><br>
   * 对日期的【天】进行加/减
   *
   * @MethodName: addDateDays
   * @param date: 日期
   * @param days: 天数，负数为减
   * @return Date: 返回日期 加/减几天后的日期
   */
  public static Date addDateDays(Date date, int days) {
	DateTime dateTime = new DateTime(date);
    return dateTime.plusDays(days).toDate();
  }


  /**
   * <b>Description:</b><br>
   * 对日期的【周】进行加/减
   *
   * @MethodName: addDateWeeks
   * @param date: 日期
   * @param weeks: 周数，负数为减
   * @return Date: 返回日期 加/减几周后的日期
   */
  public static Date addDateWeeks(Date date, int weeks) {
	DateTime dateTime = new DateTime(date);
    return dateTime.plusWeeks(weeks).toDate();
  }


  /**
   * <b>Description:</b><br>
   * 对日期的【月】进行加/减
   *
   * @MethodName: addDateMonths
   * @param date: 日期
   * @param months: 月数
   * @return Date: 返回日期 加/减几月后的日期
   */
  public static Date addDateMonths(Date date, int months) {
	DateTime dateTime = new DateTime(date);
    return dateTime.plusMonths(months).toDate();
  }

  /**
   * <b>Description:</b><br>
   * 对日期的【年】进行加/减
   *
   * @MethodName: addDateYears
   * @param date: 日期
   * @param years: 年数
   * @return Date: 返回日期 加/减几年后的日期
   */
  public static Date addDateYears(Date date, int years) {
	DateTime dateTime = new DateTime(date);
    return dateTime.plusYears(years).toDate();
  }

  /**
   * <b>Description:</b><br>
   * 公共时间添加、减多少后获得最新时间。
   *
   * @MethodName: addDate
   * @param date: 日期 时间
   * @param addValue: 加/减的数字（
   * @param addDateType:加/加 枚举类型
   * @return Date: 返回日期 加/减后的日期
   */
  public static Date addDate(Date date, int addValue, EnumDateType dateType) {

	DateTime dateTime = new DateTime(date);
	Date result = date;
    switch (dateType) {
      case SECONDS:
        result = dateTime.plusSeconds(addValue).toDate();
        break;
      case MINUTES:
        result = dateTime.plusMinutes(addValue).toDate();
        break;
      case HOURS:
        result = dateTime.plusHours(addValue).toDate();
        break;
      case DAYS:
        result = dateTime.plusDays(addValue).toDate();
        break;
      case WEEKS:
        result = dateTime.plusWeeks(addValue).toDate();
        break;
      case MONTHS:
        result = dateTime.plusMonths(addValue).toDate();
        break;
      case YEARS:
        result = dateTime.plusYears(addValue).toDate();
        break;
      default:
        break;
    }
    return result;
  }

  /**
   * <b>Description:</b><br>
   * 计算2个日期之间相差的 相差多少年月日 比如：2011-02-02 到 2017-03-02 相差 6年，1个月，0天
   * 
   * @MethodName: dayComparePrecise
   * @param fromDate:开始日期
   * @param toDate：比较 日期
   * @return DayCompare: 返回DayCompare
   */
  public static DayCompare dayComparePrecise(Date startDate, Date endDate) {
    Calendar from = Calendar.getInstance();
    from.setTime(startDate);
    Calendar to = Calendar.getInstance();
    to.setTime(endDate);

    int fromYear = from.get(Calendar.YEAR);
    int fromMonth = from.get(Calendar.MONTH);
    int fromDay = from.get(Calendar.DAY_OF_MONTH);

    int toYear = to.get(Calendar.YEAR);
    int toMonth = to.get(Calendar.MONTH);
    int toDay = to.get(Calendar.DAY_OF_MONTH);
    int year = toYear - fromYear;
    int month = toMonth - fromMonth;
    int day = toDay - fromDay;
    return new DayCompare(year, month, day);
  }

  /**
   * <b>Description:</b><br>
   * 计算2个日期之间相差的 以年、月、日为单位，各自计算结果是多少 比如：2011-02-02 到 2017-03-02 以年为单位相差为：6年 以月为单位相差为：73个月
   * 以日为单位相差为：2220天
   *
   * @MethodName: dayCompare
   * @param startDate: 开始日期
   * @param endDate ： 比较日期
   * @return DayCompare: 返回DayCompare
   */
  public static DayCompare dayCompare(Date startDate, Date endDate) {
    Calendar from = Calendar.getInstance();
    from.setTime(startDate);
    Calendar to = Calendar.getInstance();
    to.setTime(endDate);
    // 只要年月
    int fromYear = from.get(Calendar.YEAR);
    int fromMonth = from.get(Calendar.MONTH);

    int toYear = to.get(Calendar.YEAR);
    int toMonth = to.get(Calendar.MONTH);

    int year = toYear - fromYear;
    int month = toYear * 12 + toMonth - (fromYear * 12 + fromMonth);
    int day = (int) ((to.getTimeInMillis() - from.getTimeInMillis()) / (24 * 3600 * 1000));
    return new DayCompare(year, month, day);
  }


}
