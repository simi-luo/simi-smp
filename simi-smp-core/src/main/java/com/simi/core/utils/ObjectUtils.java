package com.simi.core.utils;

import com.alibaba.fastjson.JSON;
import com.simi.core.exception.SimiException;
 

/**
* <b>Description:</b><br>
*  Object 对象工具类
*
* @author:  lhj58
* @version:  1.0
* @Note
* <b>ProjectName:</b> simi-smp-core
* <br><b>PackageName:</b> com.simi.core.utils
* <br><b>ClassName:</b> ObjectUtils
* <br><b>Date:</b> 2018年6月1日 下午10:10:35
*/
public class ObjectUtils {

  /**   
  * <b>Description:</b><br>
  *     判定对象类型是否是其中一种：
  *     对象是Integer 对象类型吗？
  *     对象是Long 对象类型吗？
  *     对象是Float 对象类型吗？
  *     对象是Double 对象类型吗？
  *     对象是Boolean 对象类型吗？
  *     对象是String 对象类型吗？ 
  * @MethodName: isObjectType  
  * @param object: 被判定的对象  
  * @return boolean: 返回true or false; 
  */
  public static boolean isObjectType(Object object) {
    return object instanceof Integer || object instanceof Long || object instanceof Float
        || object instanceof Double || object instanceof Boolean || object instanceof String;
  }
  
  
  /**   
   * <b>Description:</b><br>
   * 把Object转成JSON数据
   *
   * @MethodName: toJson  
   * @param object:对象 
   * @return String: 返回json字符串数据 
   */
  public static String toJson(Object object) {
     if (ObjectUtils.isObjectType(object)) {
       return String.valueOf(object);
     }
     return JSON.toJSONString(object);
   }

  
  public static void isNull(Object object, String message) {
      if (null == object) {
          throw new SimiException(message);
      }
  }
  
}
