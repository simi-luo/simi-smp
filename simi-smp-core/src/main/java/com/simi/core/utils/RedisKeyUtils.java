package com.simi.core.utils;

import com.simi.core.enums.EnumSysKey;

/**
* <b>Description:</b><br>
*  使用Redis时 规范Key的工具类
*
* @author:  lhj58
* @version:  1.0
* @Note
* <b>ProjectName:</b> simi-smp-core
* <br><b>PackageName:</b> com.simi.core.utils
* <br><b>ClassName:</b> RedisKeyUtils
* <br><b>Date:</b> 2018年6月2日 上午12:03:27
*/
 
public class RedisKeyUtils {
  
  private static final String COM_TAG = "com:";


  /**   
  * <b>Description:</b><br>
  * 构造生产一个测试Key
  *
  * @MethodName: keyTest  
  * @param key: 缓存数据的后缀key值， 不能为空 :key 前缀  
  * @return String: 返回字符串 ，格式如：  key:test:com:{key}
  */
  public static String keyTest(String key) {
    StringBuilder sb = new StringBuilder();
    sb.append("key:test:").append(COM_TAG).append(key);
    return sb.toString();
  }
  
  /**   
  * <b>Description:</b><br>
  * 规范Value的 Key值 
  *
  * @MethodName: keyValue  
  * @param sys: 业务系统Key前缀枚举
  * @param key: 缓存数据的后缀key值， 不能为空 : 缓存数据的后缀key值， 不能为空  
  * @return String: 返回字符串 ，格式如：  {sys.name}value:{COM_TAG}{key} 
  */
  public static String keyValue(EnumSysKey sys,String key) {
   return keyValue(sys,COM_TAG,key);
  }
  

  /**   
  * <b>Description:</b><br>
  * 规范Value的 Key值
  * 
  * @MethodName: keyValue  
  * @param sys: 业务系统Key前缀枚举 
  * @param tag: 中间tag标记,标准操作功能或值保持对象标记
  * @param key: 缓存数据的后缀key值， 不能为空 : 缓存数据的后缀key值， 不能为空  
  * @return String: 返回字符串 ，格式如：  {sys.name}value:{tag}{key}  
  */
  public static String keyValue(EnumSysKey sys,String tag ,String key) {
    StringBuilder sb = new StringBuilder();
    sb.append(sys.getName()).append("value:").append(tag).append(key);
    return sb.toString();
  }
  
  /**   
  * <b>Description:</b><br>
  * 规范set的 Key值
  * 
  * @MethodName: keySet  
  * @param sys: 业务系统Key前缀枚举
  * @param key: 缓存数据的后缀key值， 不能为空 : 缓存数据的后缀key值， 不能为空  
  * @return String: 返回字符串 ，格式如：  {sys.name}set:{COM_TAG}{key}   
  */
  public static String keySet(EnumSysKey sys,String key) { 
    return keySet(sys,COM_TAG,key);
  }
  

  
  /**   
  * <b>Description:</b><br>
  * 规范set的 Key值
  *
  * @MethodName: keySet  
  * @param sys: 业务系统Key前缀枚举 
  * @param tag: 中间tag标记,标准操作功能或值保持对象标记
  * @param key: 缓存数据的后缀key值， 不能为空 : 缓存数据的后缀key值， 不能为空 
  * @return String: 返回字符串 ，格式如：  {sys.name}set:{tag}{key}    
  */
  public static String keySet(EnumSysKey sys,String tag,String key) {
    StringBuilder sb = new StringBuilder();
    sb.append(sys.getName()).append("set:").append(key);
    return sb.toString();
  }


  /**   
  * <b>Description:</b><br>
  * 规范map的 Key值
  *
  * @MethodName: keyMap 
  * @param sys: 业务系统Key前缀枚举
  * @param key: 缓存数据的后缀key值， 不能为空 : 缓存数据的后缀key值， 不能为空 
  * @return String: 返回字符串 ，格式如：  {sys.name}map:{COM_TAG}{key}     
  */
  public static String keyHash(EnumSysKey sys,String key) {
    return keyHash(sys,COM_TAG,key);
  }

  /**   
  * <b>Description:</b><br>
  * 规范map的 Key值
  *
  * @MethodName: keyMap  
  * @param sys: 业务系统Key前缀枚举 
  * @param tag: 中间tag标记,标准操作功能或值保持对象标记
  * @param key: 缓存数据的后缀key值， 不能为空 : 缓存数据的后缀key值， 不能为空 
  * @return String: 返回字符串 ，格式如：  {sys.name}map:{tag}{key}  
  */
  public static String keyHash(EnumSysKey sys,String tag,String key) {
    StringBuilder sb = new StringBuilder();
    sb.append(sys.getName()).append("map:").append(key);
    return sb.toString();
  }
  

  /**   
  * <b>Description:</b><br>
  * 规范list的 Key值
  *
  * @MethodName: keyList
  * @param sys: 业务系统Key前缀枚举
  * @param key: 缓存数据的后缀key值， 不能为空 : 缓存数据的后缀key值， 不能为空 
  * @return String: 返回字符串 ，格式如：  {sys.name}list:{COM_TAG}{key}   
  */
  public static String keyList(EnumSysKey sys,String key) {
    return keyList(sys,COM_TAG,key);
  }

  /**   
  * <b>Description:</b><br>
  * 规范list的 Key值
  *
  * @MethodName: keyList
  * @param sys: 业务系统Key前缀枚举 
  * @param tag: 中间tag标记,标准操作功能或值保持对象标记
  * @param key: 缓存数据的后缀key值， 不能为空 : 缓存数据的后缀key值， 不能为空 
  * @return String: 返回字符串 ，格式如：  {sys.name}list:{tag}{key}    
  */
  public static String keyList(EnumSysKey sys,String tag,String key) {
    StringBuilder sb = new StringBuilder();
    sb.append(sys.getName()).append("list:").append(tag).append(key);
    return sb.toString();
  }
}
