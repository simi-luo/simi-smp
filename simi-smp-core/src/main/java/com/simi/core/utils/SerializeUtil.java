package com.simi.core.utils;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils; 

/**
* <b>Description:</b><br>
* SerializeUtil 工具类
*
* @author:  holele
* @version:  1.0
* @Note
* <b>ProjectName:</b> simi-smp-core
* <br><b>PackageName:</b> com.simi.core.utils
* <br><b>ClassName:</b> SerializeUtil
* <br><b>Date:</b> 2018年6月2日 下午5:58:36
*/ 
public class SerializeUtil {
 

    /**   
    * <b>Description:</b><br>
    * serialize 对接转换成 Byte[]
    *
    * @MethodName: serialize  
    * @param list  
    * @return byte[]: 返回类型  
    */
    public static <T> byte[] serialize(List<T> list) {
        if (list == null) {
            throw new NullPointerException("Can't serialize null");
        }
        byte[] rv = null;
        ByteArrayOutputStream bos = null;
        ObjectOutputStream os = null;
        try {
            bos = new ByteArrayOutputStream();
            os = new ObjectOutputStream(bos);
            for (T vo : list) {
                os.writeObject(vo);
            }
            os.writeObject(null);
            os.close();
            bos.close();
            rv = bos.toByteArray();
        } catch (IOException e) {
            throw new IllegalArgumentException("Non-serializable object", e);
        } finally {
            IOUtils.closeQuietly(os);
            IOUtils.closeQuietly(bos);
        }
        return rv;
    }

    /**   
    * <b>Description:</b><br>
    * serialize 对接转换成List
    * 
    * @MethodName: deserializeToList  
    * @param in  
    * @return List<T>: 返回类型  
    */
    public static <T> List<T> deserializeToList(byte[] in) {
        List<T> list = new ArrayList<T>();
        ByteArrayInputStream bis = null;
        ObjectInputStream is = null;
        try {
            if (in != null) {
                bis = new ByteArrayInputStream(in);
                is = new ObjectInputStream(bis);
                while (true) {
                    @SuppressWarnings("unchecked")
                    T vo = (T) is.readObject();
                    if (vo == null) {
                        break;
                    } else {
                        list.add(vo);
                    }
                }
                is.close();
                bis.close();
            }
        } catch (IOException e) {
            System.out.println("Caught IOException decoding  bytes of data"); 
        } catch (ClassNotFoundException e) {
          System.out.println("Caught CNFE decoding  bytes of data");   
        } finally {
            IOUtils.closeQuietly(is);
            IOUtils.closeQuietly(bis);
        }
        return list;
    }

    /**   
    * <b>Description:</b><br>
    * 序列化对接转换
    * 
    * @MethodName: serialize  
    * @param value  
    * @return byte[]: 返回类型  
    */
    public static <T> byte[] serialize(T value) {
        if (value == null) {
            throw new NullPointerException("Can't serialize null");
        }
        byte[] rv = null;
        ByteArrayOutputStream bos = null;
        ObjectOutputStream os = null;
        try {
            bos = new ByteArrayOutputStream();
            os = new ObjectOutputStream(bos);
            os.writeObject(value);
            os.close();
            bos.close();
            rv = bos.toByteArray();
        } catch (IOException e) {
            throw new IllegalArgumentException("Non-serializable object", e);
        } finally {
            IOUtils.closeQuietly(os);
            IOUtils.closeQuietly(bos);
        }
        return rv;
    }

  
    /**   
    * <b>Description:</b><br>
    * 序列化对接转换
    * 
    * @MethodName: deserialize  
    * @param in
    * @return: 说明 
    * @return T: 返回类型  
    */
    @SuppressWarnings("unchecked")
    public static <T> T deserialize(byte[] in) {
        T rv = null;
        ByteArrayInputStream bis = null;
        ObjectInputStream is = null;
        try {
            if (in != null) {
                bis = new ByteArrayInputStream(in);
                is = new ObjectInputStream(bis);
                rv = (T) is.readObject();
                is.close();
                bis.close();
            }
        } catch (IOException e) {

          System.out.println("Caught IOException decoding  bytes of data"); 
        } catch (ClassNotFoundException e) { 
          System.out.println("Caught CNFE decoding  bytes of data");   
        } finally {
            IOUtils.closeQuietly(is);
            IOUtils.closeQuietly(bis);
        }
        return rv;
    }

}

