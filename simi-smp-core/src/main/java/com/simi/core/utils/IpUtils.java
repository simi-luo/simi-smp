package com.simi.core.utils;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 * @author holele
 */
public class IpUtils {
  private static Logger logger = LoggerFactory.getLogger(IpUtils.class);

  /**
   * 描述： 获取IP地址
   * 
   * @param request:请求对象
   * @return
   */
  public static String getIpAddr(HttpServletRequest request) {
    String ip = null;
    String unknown = "unknown";
    try {
      ip = request.getHeader("x-forwarded-for");
      if (StringUtils.isEmpty(ip) || unknown.equalsIgnoreCase(ip)) {
        ip = request.getHeader("Proxy-Client-IP");
      }
      if (StringUtils.isEmpty(ip) || ip.length() == 0 || unknown.equalsIgnoreCase(ip)) {
        ip = request.getHeader("WL-Proxy-Client-IP");
      }
      if (StringUtils.isEmpty(ip) || unknown.equalsIgnoreCase(ip)) {
        ip = request.getHeader("HTTP_CLIENT_IP");
      }
      if (StringUtils.isEmpty(ip) || unknown.equalsIgnoreCase(ip)) {
        ip = request.getHeader("HTTP_X_FORWARDED_FOR");
      }
      if (StringUtils.isEmpty(ip) || unknown.equalsIgnoreCase(ip)) {
        ip = request.getRemoteAddr();
      }
    } catch (Exception e) {
      logger.error("IPUtils ERROR ", e);
    }

    // //使用代理，则获取第一个IP地址
    // if(StringUtils.isEmpty(ip) && ip.length() > 15) {
    // if(ip.indexOf(",") > 0) {
    // ip = ip.substring(0, ip.indexOf(","));
    // }
    // }

    return ip;
  }

}
