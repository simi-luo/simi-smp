package com.simi.core.utils;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Component; 

 
/**
* <b>Description:</b><br>
*  ReidsUtils 操作工具类
*
* @author:  lhj58
* @version:  1.0
* @Note
* <b>ProjectName:</b> simi-smp-core
* <br><b>PackageName:</b> com.simi.core.utils
* <br><b>ClassName:</b> RedisUtils
* <br><b>Date:</b> 2018年6月1日 下午9:53:36
*/ 
 
@SuppressWarnings("unchecked")
@Component
public class RedisUtils {
	
  /**  
 * redisTemplate:redisTemplate
 */
  @SuppressWarnings("rawtypes")
  @Autowired
  private RedisTemplate redisTemplate;
  
  @Resource(name = "redisTemplate")
  private ValueOperations<String, String> redisOptValue;
  
  @Resource(name = "redisTemplate")
  private SetOperations<String, Object> redisOptSet;
  
  @Resource(name = "redisTemplate")
  private ZSetOperations<String, Object> redisOptZSet;
  
  @Resource(name = "redisTemplate")
  private HashOperations<String, String, Object> redisOptHash;
  
  @Resource(name = "redisTemplate")
  private ListOperations<String, Object> redisOptList;
  
  
   
  /** @Fields DEFAULT_EXPIRE :  默认过期时长，单位：秒    */  
  public final static long REDIS_EXPIRE = 60 * 60 * 24; 
  /** @Fields NOT_EXPIRE :  不设置过期时长  */  
  public final static long NOT_EXPIRE = -1;

 

  /**   
  * <b>Description:</b><br>
  * 把数据set到Redis服务中
  *
  * @MethodName: set  
  * @param key：缓存数据key   key 不能为空
  * @param value:缓存数据的Value值 
  * @return void: 返回类型  
  */
  public void set(String key, Object value) {
    set(key, value, REDIS_EXPIRE);
  }
  
  /**   
  * <b>Description:</b><br>
  * 把数据set到Redis服务中
  *
  * @MethodName: set  
  * @param key：缓存数据key   key 不能为空
  * @param value:缓存数据的Value值
  * @param expire: 缓存数据的过去时间 
  * @return void: 返回类型  
  */
  public void set(String key, Object value, long expire) {
    redisOptValue.set(key, ObjectUtils.toJson(value));
    if (expire != NOT_EXPIRE) {
      redisTemplate.expire(key, expire, TimeUnit.SECONDS);
    }
  }

  /**   
  * <b>Description:</b><br>
  * 从Redis中获取缓存的数据 
  *
  * @MethodName: get  
  * @param key：缓存数据key   key 不能为空
  * @param clazz: 泛型；类对象
  * @param expire: 缓存数据的过去时间  
  * @return T: 返回类型  
  */
  public <T> T get(String key, Class<T> clazz, long expire) {
    String value = redisOptValue.get(key);
    if (expire != NOT_EXPIRE) {
      redisTemplate.expire(key, expire, TimeUnit.SECONDS);
    }
    return value == null ? null : ClassUtils.fromJson(value, clazz);
  }

  /**   
  * <b>Description:</b><br>
  * 从Redis中获取缓存的数据 
  * 
  * @MethodName: get  
  * @param key：缓存数据key   key 不能为空
  * @param clazz: 泛型；类对象 
  * @return T: 返回类型  
  */
  public <T> T get(String key, Class<T> clazz) {
    return get(key, clazz, NOT_EXPIRE);
  }

  /**   
  * <b>Description:</b><br>
  * 从Redis中获取缓存的数据 
  * 
  * @MethodName: get  
  * @param key：缓存数据key   key 不能为空
  * @param expire: 缓存数据的过去时间  
  * @return String: 返回类型  
  */
  public String get(String key, long expire) {
    String value = redisOptValue.get(key);
    if (expire != NOT_EXPIRE) {
      redisTemplate.expire(key, expire, TimeUnit.SECONDS);
    }
    return value;
  }

  /**   
  * <b>Description:</b><br>
  * 从Redis中获取缓存的数据 
  * 
  * @MethodName: get  
  * @param key：缓存数据key    key 不能为空
  * @return String: 返回类型  
  */
  public String get(String key) {
    return get(key, NOT_EXPIRE);
  }

  /**   
  * <b>Description:</b><br>
  * 把缓存数据从Redis中删除掉
  *
  * @MethodName: delete  
  * @param key: 缓存数据的key   key 不能为空
  * @return void: 返回类型  
  */
  public void delete(String key) {
    redisTemplate.delete(key);
  }
  
  
  /**   
  * <b>Description:</b><br>
  * 把数据set到Redis服务中
  *
  * @MethodName: setObject  
  * @param key：缓存数据key   key 不能为空
  * @param value:缓存数据的Value值
  * @param expire: 缓存数据的过去时间 
  * @return void: 返回类型  
  */
  public void setSet(String key, Set<?> values, long expire) {
    redisOptSet.add(key, values);
    if (expire != NOT_EXPIRE) {
      redisTemplate.expire(key, expire, TimeUnit.SECONDS);
    }
  }
 
  /**   
   * <b>Description:</b><br>
   * 把数据set到Redis服务中
   *
   * @MethodName: setObject  
   * @param key：缓存数据key   key 不能为空
   * @param value:缓存数据的Value值 
   * @return void: 返回类型  
   */
   public void setSet(String key, Set<?> value) {
     setSet(key, value, NOT_EXPIRE);
   }
   
   /**   
  * <b>Description:</b><br>
  * 获取Redis缓存Set中的数据
  *
  * @MethodName: getSet  
  * @param key：缓存key  key 不能为空  
  * @return Object: 返回类型  
  */
  public Set<?> getSet(String key){
     return redisTemplate.opsForSet().members(key);
  }
  
   
  
  /**   
  * <b>Description:</b><br>
  * 把Map<String,?>对象数据缓存到Redis Hash里面
  *
  * @MethodName: setHash  
  * @param key：缓存key key 不能为空
  * @param values： Map<String,?>  对象数据值
  * @param expire: 缓存数据 
  * @return void: 返回类型  
  */
  public void setHash(String key, Map<String,?>   value, long expire) {
    redisOptHash.putAll(key, value);
    if (expire != NOT_EXPIRE) {
      redisTemplate.expire(key, expire, TimeUnit.SECONDS);
    }
  }

  
  /**   
  * <b>Description:</b><br>
  * 把Map<String,?>对象数据缓存到Redis Hash里面
  *
  * @MethodName: setHash  
  * @param key：缓存key key 不能为空
  * @param values： Map<String,?>  对象数据值 
  * @return void: 返回类型  
  */
  public void setHash(String key, Map<String,?> value) {
    setHash(key,value,NOT_EXPIRE);
  }
   
  
  /**   
  * <b>Description:</b><br>
  * 从Redis Mash 缓存中获取数据
  *
  * @MethodName: getHash  
  * @param key：缓存key key 不能为空
  * @return: 说明 
  * @return Map<String,?>: 返回类型  
  */
  public Map<String,?> getHash(String key){
    return redisTemplate.opsForHash().entries(key);
  }
  
  
  /**   
  * <b>Description:</b><br>
  * 把数据缓存在Redis List中
  *
  * @MethodName: setList  
  * @param key：缓存key key 不能为空
  * @param values： List<?>  对象数据值
  * @param expire: 缓存过期时间 
  * @return void: 返回类型  
  */
  public void setList(String key, List<?> value,long expire) {
    redisOptList.leftPush(key, value);
    if (expire != NOT_EXPIRE) {
      redisTemplate.expire(key, expire, TimeUnit.SECONDS);
    }
  }
  
  /**   
  * <b>Description:</b><br>
  * 把数据缓存在Redis List中
  *
  * @MethodName: setList  
  * @param key：缓存key key 不能为空
  * @param values： List<?>  对象数据值 
  * @return void: 返回类型  
  */
  public void setList(String key, List<?> value) {
    setList(key, value,NOT_EXPIRE); 
  }
  
  /**   
  * <b>Description:</b><br>
  * 获取Redis list 中的数据
  *
  * @MethodName: getList  
  * @param key：缓存key key 不能为空
  * @return: 说明 
  * @return List<?>: 返回类型  
  */
  public List<?>  getList(String key){
    return (List<?>) redisTemplate.opsForList().leftPop(key); 
  }
  
  /**   
   * <b>Description:</b><br>
   * 描述这个方法的作用
   *
   * @MethodName: main  
   * @param args: 说明 
   * @return void: 返回类型  
    * @throws InterruptedException 
   */
   public static void main(String[] args) throws InterruptedException {
  //测试redis   key
//     RedisUtils redis =new  RedisUtils(); 
//     Set<String> setValue = new HashSet<String>();
//     Map<String,String> mapValue = new HashMap<String,String>();
//     List<String> listValue = new ArrayList<>();  
//     redis.set(RedisKeyUtils.keyValue(EnumSysKey.SYS_ADMIN, "user", "001"), "{}");
//     redis.setSet(RedisKeyUtils.keySet(EnumSysKey.SYS_ADMIN, "user", "001"), setValue);
//     redis.setHash(RedisKeyUtils.keyHash(EnumSysKey.SYS_ADMIN, "user", "001"), mapValue);
//     redis.setList(RedisKeyUtils.keyList(EnumSysKey.SYS_ADMIN, "user", "001"), listValue);  
   }
}
