package com.simi.core.utils;

 

/**
* <b>Description:</b><br>
*  DayCompare
*
* @author:  lhj58
* @version:  1.0
* @Note
* <b>ProjectName:</b> simi-smp-core
* <br><b>PackageName:</b> com.simi.core.utils
* <br><b>ClassName:</b> DayCompare
* <br><b>Date:</b> 2018年6月2日 上午12:46:03
*/
 
public class DayCompare{
    /** @Fields year :     */  
    private int year;
    /** @Fields month :  */  
    private int month;
    /** @Fields day   */  
    private int day;
    
    /**   
    * <b>Description:</b><br> 
    * 描述方法的作用 
    *  
    * @param year
    * @param month
    * @param day: 设定文件   
    */
    public DayCompare(int year, int month, int day) {
      super();
      this.year = year;
      this.month = month;
      this.day = day;
      System.out.println(this.toString());
    }
    
    /**   
    * <b>Description:</b><br>
    * 描述这个方法的作用
    *
    * @MethodName: getYear  
    * @return: 说明 
    * @return int: 返回类型  
    */
    public int getYear() {
      return year;
    }
    /**   
    * <b>Description:</b><br>
    * 描述这个方法的作用
    *
    * @MethodName: setYear  
    * @param year: 说明 
    * @return void: 返回类型  
    */
    public void setYear(int year) {
      this.year = year;
    }
    /**   
    * <b>Description:</b><br>
    * 描述这个方法的作用
    *
    * @MethodName: getMonth  
    * @return: 说明 
    * @return int: 返回类型  
    */
    public int getMonth() {
      return month;
    }
    /**   
    * <b>Description:</b><br>
    * 描述这个方法的作用
    *
    * @MethodName: setMonth  
    * @param month: 说明 
    * @return void: 返回类型  
    */
    public void setMonth(int month) {
      this.month = month;
    }
    public int getDay() {
      return day;
    }
    /**   
    * <b>Description:</b><br>
    * 描述这个方法的作用
    *
    * @MethodName: setDay  
    * @param day: 说明 
    * @return void: 返回类型  
    */
    public void setDay(int day) {
      this.day = day;
    }
    
    /** 
    * <b>Description: </b><br>
    * 描述
    * @MethodName: toString
    * @return  String
    * @see java.lang.Object#toString()    
     */  
    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder(); 
      sb.append(this.year).append("年");
      sb.append(this.month).append("月");
      sb.append(this.day).append("天");
      return sb.toString();
    }
       
    
    
}